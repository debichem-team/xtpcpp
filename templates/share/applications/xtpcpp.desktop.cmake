#https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html
[Desktop Entry]
Version=1.0
Name=X!TandemPipeline C++
Categories=Science;Biology;Education;Qt;
Comment=Protein inference tool
Exec=${CMAKE_INSTALL_PREFIX}/bin/xtpcpp %f
Icon=${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/scalable/apps/xtpcpp.svg
Terminal=false
Type=Application
StartupNotify=true
Keywords=Mass spectrometry;Protein inference;Peptide;
MimeType=application/x-xtandempipeline;
