/**
 * \file tests/test_condor_status.cpp
 * \date 24/01/2022
 * \author Olivier Langella
 * \brief test condor XML parser
 */


/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/catch2-only-tests [condor] -s

#include <QDebug>
#include <QString>

#include <catch2/catch.hpp>
#include "config.h"
#include "../src/core/condor_process/condorqueueparser.h"


TEST_CASE("test condor_q xml parser", "[condor]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: condor XML parser ::..", "[condor]")
  {

    CondorQueueParser condor_parser;

    condor_parser.setCondorJobSize(6);
    bool read_ok = condor_parser.readFile(
      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/condor_status/empty_queue.xml"));
    REQUIRE(condor_parser.errorString().toStdString() == "");
    REQUIRE(read_ok);
    // REQUIRE(tandem_info_parser.errorString().toStdString() == "");
    REQUIRE(condor_parser.countCondorJobStatus(CondorJobStatus::Completed) ==
            6);

    REQUIRE(condor_parser.countCondorJobStatus(CondorJobStatus::Held) == 0);


    read_ok = condor_parser.readFile(
      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/condor_status/held_queue.xml"));
    REQUIRE(condor_parser.errorString().toStdString() == "");
    REQUIRE(read_ok);
    // REQUIRE(tandem_info_parser.errorString().toStdString() == "");
    REQUIRE(condor_parser.countCondorJobStatus(CondorJobStatus::Completed) ==
            3);

    REQUIRE(condor_parser.countCondorJobStatus(CondorJobStatus::Held) == 3);


    QString condor_q_xml = "<?xml version=\"1.0\"?><!DOCTYPE classads SYSTEM "
                           "\"classads.dtd\"><classads></classads>";

    condor_parser.read(condor_q_xml);


    REQUIRE(condor_parser.errorString().toStdString() == "");
    REQUIRE(read_ok);
    // REQUIRE(tandem_info_parser.errorString().toStdString() == "");
    REQUIRE(condor_parser.countCondorJobStatus(CondorJobStatus::Completed) ==
            6);

    REQUIRE(condor_parser.countCondorJobStatus(CondorJobStatus::Held) == 0);
  }
}
