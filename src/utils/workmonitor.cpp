/**
 * \file utils/workmonitor.cpp
 * \date 10/6/2017
 * \author Olivier Langella
 * \brief monitoring progress in any worker thread
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "workmonitor.h"
#include <QDebug>
#include "../gui/mainwindow.h"

WorkMonitor::WorkMonitor(MainWindow *main_window)
{
  mp_mainWindow   = main_window;
  m_totalSteps    = 100;
  m_timerDuration = 1000;
  m_time.restart();
  mp_subMonitor = new SubMonitor(this);
}

void
WorkMonitor::finished(const QString &message)
{
  emit workerJobFinished(message);
}

void
WorkMonitor::canceled(const QString &message)
{
  emit workerJobCanceled(message);
}

void
WorkMonitor::message(const QString &message)
{
  if(m_timerCount.isValid())
    {
      if(m_time.elapsed() > m_timerDuration)
        {
          m_time.restart();
          emit workerMessage(message);
        }
    }
  else
    {
      m_timerCount.start();
    }
}
void
WorkMonitor::message(const QString &message, int value)
{
  qDebug() << value << " " << m_totalSteps << " "
           << (value / m_totalSteps) * 100;
  if(m_timerCount.isValid())
    {
      if(m_time.elapsed() > m_timerDuration)
        {
          int percent = ((float)value / (float)m_totalSteps) * (float)100;

          m_time.restart();
          emit workerMessagePercent(message, percent);
        }
    }
  else
    {
      m_timerCount.start();
    }
}

void
WorkMonitor::setTotalSteps(std::size_t total_number_of_steps)
{
  pappso::UiMonitorInterface::setTotalSteps(total_number_of_steps);
  m_count = 0;
}

void
WorkMonitor::count()
{
  m_count++;
  if(m_timerCount.isValid())
    {
      if(m_timerCount.elapsed() > m_timerDuration)
        {
          qDebug() << m_count << " " << m_totalSteps << " "
                   << (m_count / m_totalSteps) * 100;
          int percent = ((float)m_count / (float)m_totalSteps) * (float)100;

          m_timerCount.restart();
          emit workerPercent(percent);
        }
    }
  else
    {
      m_timerCount.start();
    }
}


void
WorkMonitor::appendText(const QString &message)
{
  qDebug();

  m_bufferMessage.append(message);

  if(m_timerCount.isValid())
    {
      if(m_timerCount.elapsed() > m_timerDuration)
        {
          qDebug();

          m_timerCount.restart();
          emit workerAppendQString(m_bufferMessage);
          m_bufferMessage.clear();
        }
    }
  else
    {
      m_timerCount.start();
      emit workerAppendQString(m_bufferMessage);
      m_bufferMessage.clear();
    }
}

void
WorkMonitor::setText(const QString text)
{
  emit workerSetText(text);
}

void
WorkMonitor::setStatus(const QString &status)
{
  message(status);
}
void
WorkMonitor::setTitle(const QString &title)
{
  message(title);
}
bool
WorkMonitor::shouldIstop()
{
  if(m_timerStop.isValid())
    {
      if(m_timerStop.elapsed() > m_timerDuration)
        {
          qDebug();
          m_timerStop.restart();
          emit showStopButton();
          return mp_mainWindow->stopWorkerThread();
        }
    }
  else
    {
      m_timerStop.start();
    }
  return false;
}

SubMonitor::SubMonitor(WorkMonitor *main_monitor)
{
  mp_workMonitor  = main_monitor;
  m_timerDuration = 1000;
  // m_time.restart();
}


SubMonitor &
WorkMonitor::getSubMonitor()
{
  return *mp_subMonitor;
}

void
SubMonitor::appendText(const QString &text)
{
  qDebug() << text;
  mp_workMonitor->appendText(text);
}
void
SubMonitor::setStatus(const QString &status)
{
  mp_workMonitor->appendText(QString("%1\n").arg(status));
}
void
SubMonitor::setTitle(const QString &title)
{
  mp_workMonitor->appendText(title);
}
bool
SubMonitor::shouldIstop()
{
  qDebug();
  return mp_workMonitor->shouldIstop();
}

void
SubMonitor::count()
{
  m_count++;
  if(m_timerStatus.isValid())
    {
      if(m_timerStatus.elapsed() > m_timerDuration)
        {
          m_timerStatus.restart();

          int percent = ((float)m_count / (float)m_totalSteps) * (float)100;
          emit mp_workMonitor->workerPercent(percent);
        }
    }
  else
    {
      m_timerStatus.start();
    }
}

void
SubMonitor::setTotalSteps(std::size_t total_number_of_steps)
{
  pappso::UiMonitorInterface::setTotalSteps(total_number_of_steps);
  m_count = 0;
}
