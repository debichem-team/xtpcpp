
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/
#include <QUrl>
#include <QString>
#include "types.h"
#include <pappsomspp/types.h>
#include <pappsomspp/amino_acid/aamodification.h>

#pragma once
class Utils
{
  public:
  static const QUrl getOlsUrl(QString modification);
  static const QString getIdentificationEngineName(IdentificationEngine engine);
  static const QString getDatabaseName(ExternalDatabase database);
  static const QString getXmlDouble(pappso::pappso_double number);
  static const QString checkXtandemVersion(const QString &tandem_bin_path);
  static const QString checkMassChroQVersion(const QString &masschroq_bin_path);
  static const QString checkRVersion(const QString r_version);
  static double computeFdr(std::size_t count_decoy, std::size_t count_target);
  static pappso::AaModificationP
  guessAaModificationPbyMonoisotopicMassDelta(pappso::pappso_double mass);
  static pappso::AaModificationP
  translateAaModificationFromUnimod(const QString &unimod_accession);
  static std::vector<std::pair<pappso::pappso_double, size_t>>
  getHistogram(std::vector<pappso::pappso_double> data_values,
               unsigned int number_of_class);
  static pappso::MzFormat guessDataFileFormatFromFile(const QString &filename);
  static void transformPlainNewLineToHtmlFormat(QString &transform_line);
};
