/**
 * \file utils/msrunstatisticshandler.cpp
 * \date 12/08/2018
 * \author Olivier Langella
 * \brief handler on MZ data file to read all spectrums and make basic
 * statistics
 */


/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

#include "msrunstatisticshandler.h"
#include <pappsomspp/msrun/msrunreader.h>
#include <QDebug>

MsRunStatisticsHandler::~MsRunStatisticsHandler()
{
}
bool
MsRunStatisticsHandler::needPeakList() const
{
  return true;
}
void
MsRunStatisticsHandler::setQualifiedMassSpectrum(
  const pappso::QualifiedMassSpectrum &qspectrum)
{
  unsigned int ms_level = qspectrum.getMsLevel();
  if(ms_level == 0)
    return;
  if(ms_level > _count_ms_level_spectrum.size())
    {
      _count_ms_level_spectrum.resize(ms_level, 0);
      _tic_ms_level_spectrum.resize(ms_level, 0);
    }
  _count_ms_level_spectrum[ms_level - 1]++;

  pappso::MassSpectrumCstSPtr spectrum_sp = qspectrum.getMassSpectrumCstSPtr();
  if(spectrum_sp != nullptr)
    {
      for(auto &&peak : *(spectrum_sp.get()))
        {

          _tic_ms_level_spectrum[ms_level - 1] += peak.y;
        }
    }
}

unsigned long
MsRunStatisticsHandler::getMsLevelCount(unsigned int ms_level) const
{
  if(ms_level == 0)
    return 0;
  if(ms_level > _count_ms_level_spectrum.size())
    return 0;
  return (_count_ms_level_spectrum[ms_level - 1]);
}

pappso::pappso_double
MsRunStatisticsHandler::getMsLevelTic(unsigned int ms_level) const
{
  if(ms_level == 0)
    return 0;
  if(ms_level > _tic_ms_level_spectrum.size())
    return 0;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << _tic_ms_level_spectrum[ms_level - 1];
  return (_tic_ms_level_spectrum[ms_level - 1]);
}

unsigned long
MsRunStatisticsHandler::getTotalCount() const
{
  unsigned long total = 0;
  for(unsigned long count : _count_ms_level_spectrum)
    {
      total += count;
    }
  return total;
}
