/**
 * \file utils/peptidestore.h
 * \date 7/10/2016
 * \author Olivier Langella
 * \brief store unique version of peptides
 */

/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of peptider.
 *
 *     peptider is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     peptider is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with peptider.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../core/peptidextp.h"
#include <unordered_map>
#include <set>
#include "../core/labeling/labelingmethod.h"

class PeptideStore
{
  public:
  PeptideStore();
  ~PeptideStore();

  PeptideXtpSp &getInstance(PeptideXtpSp &peptide_in);

  /** @brief check that modifications are coded with PSI MOD accessions
   */
  bool checkPsimodCompliance() const;

  const std::set<pappso::AaModificationP> &getModificationCollection() const;

  /** @brief replaces all occurences of a given modification by another one
   */
  bool replaceModification(pappso::AaModificationP oldmod,
                           pappso::AaModificationP newmod);

  /** @brief apply labeling method to all peptide match
   * */
  void setLabelingMethodSp(LabelingMethodSp labeling_method_sp);

  const std::unordered_map<std::size_t, PeptideXtpSp> &getPeptideMap() const;
  std::size_t size() const;

  private:
  /** @brief reset labeling method
   * */
  void clearLabelingMethod();

  private:
  bool m_checkSequenceCrc = false;
  LabelingMethodSp _labeling_method_sp;
  std::hash<std::string> _hash_fn;
  std::unordered_map<std::size_t, PeptideXtpSp> _map_crc_peptide_list;
  std::set<pappso::AaModificationP> _modification_collection;
};
