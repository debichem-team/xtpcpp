/**
 * \file utils/msrunstore.cpp
 * \date 5/4/2017
 * \author Olivier Langella
 * \brief store unique version of msrun references (filename, sample name...)
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "msrunstore.h"
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/utils.h>
#include <QFileInfo>

MsRunStore::MsRunStore()
{
}

MsRunStore::~MsRunStore()
{
}

MsRunSp
MsRunStore::getInstance(const MsRun *p_msrun)
{
  auto it = std::find_if(
    _map_msrun.begin(), _map_msrun.end(), [p_msrun](const MsRunSp &msrun_sp) {
      return msrun_sp.get() == p_msrun;
    });
  if(it != _map_msrun.end())
    {
      return (*it);
    }
  MsRunSp p_xtfile;
  return p_xtfile;
}

MsRunSp
MsRunStore::getInstance(const QString &location)
{
  QFileInfo file_ondisk(location);
  auto it = _map_msrun.end();
  if(file_ondisk.baseName() == location)
    {
      // look only for the same sample name
      it = std::find_if(_map_msrun.begin(),
                        _map_msrun.end(),
                        [location](const MsRunSp &msrun_sp) {
                          return msrun_sp.get()->getSampleName() == location;
                        });
    }
  else
    {
      // look for an absolute file path
      it = std::find_if(_map_msrun.begin(),
                        _map_msrun.end(),
                        [location](const MsRunSp &msrun_sp) {
                          if(msrun_sp.get()->getSampleName() ==
                             msrun_sp.get()->getFileName())
                            {
                              return msrun_sp.get()->getSampleName() ==
                                     QFileInfo(location).baseName();
                            }
                          return msrun_sp.get()->getFileName() == location;
                        });
    }
  if(it != _map_msrun.end())
    {
      return *it;
    }
  else
    {
      MsRunSp p_xtfile = std::make_shared<MsRun>(location);
      p_xtfile.get()->setXmlId(QString("msrun%1").arg(
        pappso::Utils::getLexicalOrderedString(_map_msrun.size() + 1)));
      p_xtfile.get()->setFileName(location);
      _map_msrun.push_back(p_xtfile);
      return p_xtfile;
    }
  throw pappso::ExceptionNotFound(
    QObject::tr("Ms resource %1 not found").arg(location));
}


const std::vector<MsRunSp> &
MsRunStore::getMsRunList() const
{
  // std::vector<MsRunSp> msrun_list;
  // for (auto & msrun_pair :_map_msrun) {
  //     msrun_list.push_back(msrun_pair.second);
  // }
  return _map_msrun;
}
