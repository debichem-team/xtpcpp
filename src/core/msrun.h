/**
 * \filed core/msrun.h
 * \date 5/4/2017
 * \author Olivier Langella
 * \brief describes an MS run (chemical sample injected in a mass spectrometer)
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include <memory>
#include <QString>
#include <QVariant>
#include <pappsomspp/msrun/msrunid.h>
#include <pappsomspp/msrun/msrunreader.h>
#include <pappsomspp/msrun/alignment/msrunretentiontime.h>
#include <pappsomspp/xicextractor/msrunxicextractorinterface.h>
#include <pappsomspp/peptide/peptide.h>
#include "../utils/types.h"
#include "../utils/peptideevidencestore.h"
#include <utils/msrunstatisticshandler.h>

class MsRunAlignmentGroup;
typedef std::shared_ptr<MsRunAlignmentGroup> MsRunAlignmentGroupSp;

class MsRun;
typedef std::shared_ptr<MsRun> MsRunSp;

class MsRun : public pappso::MsRunId
{
  public:
  MsRun(const QString &location);
  MsRun(const MsRun &other);
  ~MsRun();


  /** \brief set MS run statistics
   * any statistics on this MS run file
   */
  virtual void setMsRunStatistics(MsRunStatistics param, const QVariant &value);


  /** \brief get MS run statistics
   */
  virtual const QVariant getMsRunStatistics(MsRunStatistics param) const;

  /** \brief get MS run statistics map
   */
  virtual const std::map<MsRunStatistics, QVariant> &
  getMsRunStatisticsMap() const;

  /** \brief find the msRun file and return an msrunreader shared pointer on it
   */
  pappso::MsRunReaderSPtr findMsRunFile();

  pappso::MsRunReaderSPtr &getMsRunReaderSPtr();

  /** @brief release shared pointer on MSrun reader
   */
  void freeMsRunReaderSp();

  void checkMsRunStatistics();
  void checkMsRunStatistics(MsRunStatisticsHandler *currentHandler);

  pappso::MsRunXicExtractorInterfaceSp getMsRunXicExtractorInterfaceSp();

  void
  buildMsRunRetentionTime(const PeptideEvidenceStore &peptide_evidence_store);
  void computeMsRunRetentionTime();
  void clearMsRunRetentionTime();
  pappso::MsRunRetentionTime<const pappso::Peptide *> *
  getMsRunRetentionTimePtr();
  void setTimstofMs2CentroidParameters(const QString &centroid_parameters);
  MsRunAlignmentGroupSp getAlignmentGroup();
  void setAlignmentGroup(MsRunAlignmentGroupSp new_group);


  /** @brief get a mass spectrum from this MSrun with the given spectrum index
   * @param spectrum_index spectrum index in this MSrun
   * @return Mass Spectrum shared pointer
   */
  pappso::MassSpectrumCstSPtr
  getMassSpectrumCstSPtrBySpectrumIndex(std::size_t spectrum_index);


  /** @brief get a mass spectrum from this MSrun with the given scan number
   * the scan number is deprecated in favor of the spectrum index.
   * Spectrum index is always available (order of spectrum in the data file)
   * scan number is a given name by the constructor and not as reliable as the
   * spectrum index
   * @param scan_number the scan number
   * @return Mass Spectrum shared pointer
   */
  pappso::MassSpectrumCstSPtr
  getMassSpectrumCstSPtrByScanNumber(std::size_t scan_number);


  /** @brief get a qualified mass spectrum from this MSrun with the given
   * spectrum index
   * @param spectrum_index spectrum index in this MSrun
   * @param data boolean if true, retrieves the peak list, if false, only give
   * mass spectrum informations
   * @return QualifiedMassSpectrum
   */
  pappso::QualifiedMassSpectrum
  getQualifiedMassSpectrumBySpectrumIndex(std::size_t spectrum_index,
                                          bool data);


  /** @brief get a qualified mass spectrum from this MSrun with the given scan
   * number the scan number is deprecated in favor of the spectrum index.
   * Spectrum index is always available (order of spectrum in the data file)
   * scan number is a given name by the constructor and not as reliable as the
   * spectrum index
   * @param scan_number the scan number
   * @param data boolean if true, retrieves the peak list, if false, only give
   * mass spectrum informations
   * @return QualifiedMassSpectrum
   */
  pappso::QualifiedMassSpectrum
  getQualifiedMassSpectrumByScanNumber(std::size_t scan_number, bool data);


  /** @brief finds the spectrum index corresponding to a scan number
   *
   * @param scan_number the scan number to convert
   * @return spectrum index or an exception
   */
  std::size_t scanNumber2SpectrumIndex(std::size_t scan_number);

  private:
  void buildMsRunReaderSp();

  private:
  std::map<MsRunStatistics, QVariant> _param_stats;

  pappso::MsRunReaderSPtr _msrun_reader_sp;

  pappso::MsRunXicExtractorInterfaceSp _xic_extractor_sp;

  pappso::MsRunRetentionTime<const pappso::Peptide *> *mpa_msrunRetentionTime =
    nullptr;

  MsRunAlignmentGroupSp msp_alignmentGroup;
};


Q_DECLARE_METATYPE(MsRunSp)
