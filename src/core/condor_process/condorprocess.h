/**
 * \file /core/condor_process/condorprocess.h
 * \date 12/11/2020
 * \author Thomas Renne
 * \brief handles condor jobs
 */

/*******************************************************************************
 * Copyright (c) 2017 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QTemporaryDir>
#include "../../utils/workmonitor.h"
#include "../../gui/mainwindow.h"
#include "condorqueueparser.h"

class CondorProcess
{
  public:
  CondorProcess(MainWindow *p_main_window, WorkMonitorInterface *p_monitor,
                QString app_run);
  virtual ~CondorProcess();
  std::size_t getCondorJobSize() const;

  /** @brief display condor job status
   */
  void displayCondorJobStatus() const;

  protected:
  void prepareTemporaryDirectory();
  void parseCondorJobNumber(QString condor_job);
  void surveyCondorJob();

  private:
  bool shouldIstop();
  void getCondorJobState();
  void parseCondorQueue(QString &condor_q_xml);
  void condorRemoveJob();

  protected:
  QTemporaryDir *mp_tmpDir = nullptr;
  QString m_condorSubmitCommand;
  int m_maxTimeMs = (60000 * 60 * 24); // 1 day
  std::size_t m_condorClusterNumber;
  std::size_t m_condorJobSize;

  private:
  QString m_app_run;
  MainWindow *mp_main_window;
  WorkMonitorInterface *mp_monitor;
  QString m_condorQCommand;
  QString m_condorRmCommand;
  std::size_t m_condorStatusTimerMillisecond = 1000;
  std::size_t m_condorCompletedJobs          = 0;
  CondorQueueParser m_condoQueueParser;
};
