/**
 * \file src/core/condor_process/condorqueueparser.cpp
 * \date 24/01/2022
 * \author Olivier Langella
 * \brief parse condor_q --xml command results
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "condorqueueparser.h"

CondorQueueParser::CondorQueueParser()
{
  for(std::int8_t i = 0; i < 10; i++)
    {
      m_countStatus[i] = 0;
    }
}

CondorQueueParser::~CondorQueueParser()
{
}

void
CondorQueueParser::readStream()
{
  for(std::int8_t i = 0; i < 10; i++)
    {
      m_countStatus[i] = 0;
    }
  m_isEmpty = true;

  if(m_qxmlStreamReader.readNextStartElement())
    {
      //<classads>
      if(m_qxmlStreamReader.name() == "classads")
        {
          //<note type="input" label="output, histogram column width">30</note>
          qDebug();
          while(m_qxmlStreamReader.readNextStartElement())
            {
              //<c>
              qDebug() << m_qxmlStreamReader.name();

              if(m_qxmlStreamReader.name() == "c")
                {
                  while(m_qxmlStreamReader.readNextStartElement())
                    {
                      qDebug() << m_qxmlStreamReader.name();
                      read_a();
                    }
                  end_c();
                }
              else
                {
                  m_qxmlStreamReader.raiseError(
                    QObject::tr(
                      "Not an HTCondor condor_q xml result: classads => %1")
                      .arg(m_qxmlStreamReader.name()));
                  m_qxmlStreamReader.skipCurrentElement();
                }
            }
        }
      else
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("Not an HTCondor condor_q xml result"));
          m_qxmlStreamReader.skipCurrentElement();
        }
    }


  // finally
  std::size_t really_completed_jobs = 0;
  if(m_isEmpty)
    {
      qDebug();
      really_completed_jobs = m_totalJobs;
    }
  else
    {
      qDebug();
      int counted_jobs = 0;
      for(std::int8_t i = 0; i < 10; i++)
        {
          counted_jobs += m_countStatus[i];
        }

      int diff_jobs = m_totalJobs - counted_jobs;
      really_completed_jobs =
        m_countStatus[(std::int8_t)CondorJobStatus::Completed] + diff_jobs;
    }
  m_countStatus[(std::int8_t)CondorJobStatus::Completed] =
    really_completed_jobs;
}

std::size_t
CondorQueueParser::countCondorJobStatus(CondorJobStatus status) const
{
  return m_countStatus[(std::int8_t)status];
}


void
CondorQueueParser::read_a()
{
  //<a n="Args"><s>/gorgone/pappso/tmp/xtpcpp.RYCRaD/tandem.RSPuHb --tmp /tmp
  //--tandem /gorgone/pappso/bin/tandem-20150401</s></a>
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name() == "a")
    {
      // read_note();
      QString n = m_qxmlStreamReader.attributes().value("n").toString();
      qDebug() << m_qxmlStreamReader.name() << " n=" << n;

      // i

      if(n == "ProcId")
        {
          m_qxmlStreamReader.readNextStartElement();
          if(m_qxmlStreamReader.name() == "i")
            {
              m_currentProcId = m_qxmlStreamReader.readElementText().toInt();
            }
          m_qxmlStreamReader.skipCurrentElement();
        }
      else if(n == "JobStatus")
        {
          // <a n="JobStatus"><i>2</i></a>
          m_qxmlStreamReader.readNextStartElement();
          qDebug() << m_qxmlStreamReader.name();
          if(m_qxmlStreamReader.name() == "i")
            {
              m_currentCondorJobStatus = static_cast<CondorJobStatus>(
                m_qxmlStreamReader.readElementText().toInt());
            }
          m_qxmlStreamReader.skipCurrentElement();
          // logger.debug(currentProcId);
        }


      else if(n == "RemoteHost")
        {
          // <a n="RemoteHost"><s>slot1@proteus3</s></a>

          m_qxmlStreamReader.readNextStartElement();
          qDebug() << m_qxmlStreamReader.name();
          if(m_qxmlStreamReader.name() == "s")
            {
              m_currentRemoteHost =
                m_qxmlStreamReader.readElementText().simplified();
            }
          m_qxmlStreamReader.skipCurrentElement();
        }
      else if(n == "LastRemoteHost")
        {
          // <a n="LastRemoteHost"><s>slot1@proteus4</s></a>

          m_qxmlStreamReader.readNextStartElement();
          qDebug() << m_qxmlStreamReader.name();
          if(m_qxmlStreamReader.name() == "s")
            {
              m_currentLastRemoteHost =
                m_qxmlStreamReader.readElementText().simplified();
            }
          m_qxmlStreamReader.skipCurrentElement();
        }

      else
        {
          m_qxmlStreamReader.skipCurrentElement();
        }
    }
  else
    { // not a a
      m_qxmlStreamReader.skipCurrentElement();
    }
  qDebug();
}

void
CondorQueueParser::end_c()
{
  m_isEmpty = false;
  m_countStatus[(std::int8_t)m_currentCondorJobStatus] += 1;

  qDebug() << "currentRemoteHost:" << m_currentRemoteHost
           << " currentLastRemoteHost" << m_currentLastRemoteHost << " "
           << (std::int8_t)m_currentCondorJobStatus;
}

void
CondorQueueParser::setCondorJobSize(std::size_t total)
{
  m_totalJobs = total;
}
