/**
 * \file src/core/identification_sources/identificationmzidentmlfile.cpp
 * \date 20/1/2021
 * \author Olivier Langella
 * \brief parse mzIdentML file
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#include "identificationmzidentmlfile.h"
#include <pappsomspp/pappsoexception.h>
#include "../project.h"
#include "../../input/mzidentml/msrunsaxhandler.h"
#include "../../input/mzidentml/mzidentmlsaxhandler.h"

IdentificationMzIdentMlFile::IdentificationMzIdentMlFile(
  const QFileInfo &mzidentmlFile)
  : IdentificationDataSource(mzidentmlFile.absoluteFilePath()),
    m_mzidentmlFile(mzidentmlFile)
{
  m_identificationEngine = IdentificationEngine::unknown;
}

IdentificationMzIdentMlFile::IdentificationMzIdentMlFile(
  const IdentificationMzIdentMlFile &other)
  : IdentificationDataSource(other), m_mzidentmlFile(other.m_mzidentmlFile)
{
  m_identificationEngine = other.m_identificationEngine;
}

IdentificationMzIdentMlFile::~IdentificationMzIdentMlFile()
{
}

void
IdentificationMzIdentMlFile::parseTo(Project *p_project)
{
  qDebug();

  qDebug() << m_mzidentmlFile.absoluteFilePath() << "'";
  m_identificationEngine = IdentificationEngine::unknown;
  getMzIdentMlFileMsRunSp(p_project);
  std::vector<IdentificationGroup *> identification_list =
    p_project->getIdentificationGroupList();
  IdentificationGroup *identification_group_p = nullptr;
  if(p_project->getProjectMode() == ProjectMode::combined)
    {
      if(identification_list.size() == 0)
        {
          identification_group_p = p_project->newIdentificationGroup();
        }
      else
        {
          identification_group_p = identification_list[0];
        }
    }
  else
    {
      for(IdentificationGroup *identification_p_flist : identification_list)
        {
          if(identification_p_flist->containSample(
               msp_msRun.get()->getSampleName()))
            {
              identification_group_p = identification_p_flist;
              break;
            }
        }
      if(identification_group_p == nullptr)
        {
          identification_group_p = p_project->newIdentificationGroup();
        }
    }

  identification_group_p->addIdentificationDataSourceP(this);
  MzIdentMlSaxHandler *parser =
    new MzIdentMlSaxHandler(p_project, identification_group_p, this);

  QXmlSimpleReader simplereader;
  simplereader.setContentHandler(parser);
  simplereader.setErrorHandler(parser);

  QFile qfile(m_mzidentmlFile.absoluteFilePath());
  QXmlInputSource xmlInputSource(&qfile);

  if(simplereader.parse(xmlInputSource))
    {
      m_identificationEngine = parser->getIdentificationEngine();
      qfile.close();

      delete parser;
    }
  else
    {
      qDebug() << parser->errorString();
      // throw PappsoException(
      //    QObject::tr("error reading tandem XML result file :\n").append(
      //         parser->errorString()));

      qfile.close();

      throw pappso::PappsoException(
        QObject::tr("Error reading %1 mzIdentML file :\n %2")
          .arg(m_mzidentmlFile.absoluteFilePath())
          .arg(parser->errorString()));
    }

  qDebug();
}

void
IdentificationMzIdentMlFile::getMzIdentMlFileMsRunSp(Project *p_project)
{
  MsRunSaxHandler *parser = new MsRunSaxHandler();

  QXmlSimpleReader simplereader;
  simplereader.setContentHandler(parser);
  simplereader.setErrorHandler(parser);

  QFile qfile(m_mzidentmlFile.absoluteFilePath());
  QXmlInputSource xmlInputSource(&qfile);

  if(simplereader.parse(xmlInputSource))
    {
      MsRunSp msrun_sp = p_project->getMsRunStore().getInstance(
        parser->getSpectraDataLocation());
      setMsRunSp(msrun_sp);
      qfile.close();

      delete parser;
    }
  else
    {
      qDebug() << parser->errorString();
      // throw PappsoException(
      //    QObject::tr("error reading tandem XML result file :\n").append(
      //         parser->errorString()));

      qfile.close();

      throw pappso::PappsoException(
        QObject::tr("Error reading %1 mzIdentML file :\n %2")
          .arg(m_mzidentmlFile.absoluteFilePath())
          .arg(parser->errorString()));
    }
}
