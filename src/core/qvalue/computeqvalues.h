/**
 * \file core/qvalue/computeqvalues.h
 * \date 04/09/2019
 * \author Olivier Langella
 * \brief compute q-value for each peptide evidence (PSM) of a project
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../project.h"
#include <odsstream/calcwriterinterface.h>


/**
 * @todo write docs
 */
class ComputeQvalues
{
  public:
  /**
   * Default constructor
   */
  ComputeQvalues(Project *project_p);

  /**
   * Destructor
   */
  ~ComputeQvalues();

  void writeDistributionsByEngines(CalcWriterInterface *p_writer) const;

  private:
  bool isPeptideEvidenceDecoy(const PeptideEvidence *pep) const;

  void computeTandemPeptideEvidenceQvalues(
    std::vector<PeptideEvidence *> &tandem_peptide_evidence_list);
  void computeMascotPeptideEvidenceQvalues(
    std::vector<PeptideEvidence *> &mascot_peptide_evidence_list) const;
  void computeDeepProtPeptideEvidenceQvalues(
    std::vector<PeptideEvidence *> &deepprot_peptide_evidence_list) const;

  void cleanPeptideEvidenceList(
    std::vector<PeptideEvidence *> &peptide_evidence_list) const;

  private:
  std::vector<const PeptideEvidence *> m_falsePeptideEvidenceList;

  std::vector<PeptideEvidence *> m_tandemPeptideEvidenceList;
};
