/**
 * \file src/core/masschroq_run/masschroqfileparameters.h
 * \date 25/01/2019
 * \author Olivier Langella
 * \brief all needed information to write a MassChroqML file
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../../utils/types.h"
#include <QString>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/processing/filters/filtersuitestring.h>
#include <pappsomspp/processing/detection/tracedetectionzivy.h>
#include "../../core/msrun.h"

struct MasschroqFileParameters;
typedef std::shared_ptr<MasschroqFileParameters> MasschroqFileParametersSp;


struct ZivyParams
{
  std::shared_ptr<pappso::TraceDetectionInterface>
  newTraceDetectionZivySPtr() const;
  void saveSettings() const;
  void loadSettings();

  unsigned int _maxmin_half_window    = 3;
  unsigned int _minmax_half_window    = 2;
  double _maxmin_threshold            = 3000;
  double _minmax_threshold            = 5000;
  unsigned int _smoothing_half_window = 1;
};


struct MasschroqFileParameters
{
  void save() const;
  void load();

  /** @brief quantification result output file name*/
  // QString result_file_name;

  /** @brief quantification result output file type*/
  TableFileFormat result_file_format = TableFileFormat::ods;

  bool export_compar_file = false;
  // QString compar_file_name;
  // TableFileFormat compar_file_format = TableFileFormat::ods;

  bool write_alignment_times;
  QString alignment_times_directory;

  unsigned int ms2_tendency_half_window;
  unsigned int ms2_smoothing_half_window;
  unsigned int ms1_smoothing_half_window;

  pappso::PrecisionPtr xic_extraction_range;


  pappso::XicExtractMethod xic_extraction_method; // sum or max

  pappso::FilterSuiteStringSPtr msp_xicFilterSuiteString;

  std::vector<MsRunAlignmentGroupSp> alignment_groups;

  ZivyParams m_zivyParams;
};
