/**
 * \file /core/masschroq_run/masschroqcondorprocess.h
 * \date 12/11/2020
 * \author Thomas Renne
 * \brief handles execution of MasschroQ process through condor job
 */

/*******************************************************************************
 * Copyright (c) 2017 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "masschroqbatchprocess.h"
#include <QTemporaryDir>
#include "../condor_process/condorprocess.h"

class MassChroQCondorProcess : public MassChroQBatchProcess,
                               public CondorProcess
{
  public:
  MassChroQCondorProcess(MainWindow *p_main_window,
                         WorkMonitorInterface *p_monitor,
                         const MassChroQRunBatch masschroq_batch_param);
  virtual ~MassChroQCondorProcess();

  virtual void run();

  private:
  QString createCondorSubmitFile();


  private:
  std::size_t m_condorRequestMemory;
  std::size_t m_condorDiskUsage;
};
