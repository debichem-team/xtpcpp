/**
 * \file output/ods/comparspecificspectrasheet.h
 * \date 1/5/2017
 * \author Olivier Langella
 * \brief ODS compar specific spectra sheet
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include "comparbasesheet.h"

class ComparSpecificSpectraSheet : public ComparBaseSheet
{
  public:
  ComparSpecificSpectraSheet(OdsExport *p_ods_export,
                             CalcWriterInterface *p_writer,
                             const Project *p_project,
                             SubMonitor &sub_monitor);

  protected:
  virtual void writeComparValue(const ProteinMatch *p_protein_match,
                                ValidationState state,
                                const MsRun *p_msrun,
                                const Label *p_label = nullptr) override;
};

class ComparSpecificSequenceSheet : public ComparBaseSheet
{
  public:
  ComparSpecificSequenceSheet(OdsExport *p_ods_export,
                              CalcWriterInterface *p_writer,
                              const Project *p_project,
                              SubMonitor &sub_monitor);

  protected:
  virtual void writeComparValue(const ProteinMatch *p_protein_match,
                                ValidationState state,
                                const MsRun *p_msrun,
                                const Label *p_label = nullptr) override;
};
