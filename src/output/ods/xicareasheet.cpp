/**
 * \file /output/xicareasheet.cpp
 * \date 16/10/2020
 * \author Olivier Langella
 * \brief ODS grouping sheet
 */

/*******************************************************************************
 * Copyright (c) 2020 Thomas Renne <thomas.renne@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "xicareasheet.h"

XicAreaSheet::XicAreaSheet(CalcWriterInterface *p_writer,
                           QString project_name,
                           const std::vector<XicBox *> xic_boxs,
                           bool all_data)
{
  mp_writer = p_writer;

  mp_xicBoxList = xic_boxs;

  OdsTableSettings table_settings;
  table_settings.setVerticalSplit(1);
  mp_writer->setCurrentOdsTableSettings(table_settings);
  if(all_data)
    {
      mp_writer->writeSheet(QString("%1_xic_all_data").arg(project_name));
      writeHeadersAllData();
      for(XicBox *xic : mp_xicBoxList)
        {
          writeXicAllData(xic);
        }
    }
  else
    {
      mp_writer->writeSheet(QString("%1_xic_area").arg(project_name));
      writeHeaders();
      for(XicBox *xic : mp_xicBoxList)
        {
          writeAreaPeackdata(xic);
        }
    }
}

void
XicAreaSheet::writeHeaders()
{
  mp_writer->writeLine();
  mp_writer->setCellAnnotation("ms run name");
  mp_writer->writeCell("msrun");
  mp_writer->setCellAnnotation("area of the peak");
  mp_writer->writeCell("area");
  mp_writer->setCellAnnotation("left boundary of the peak");
  mp_writer->writeCell("rt_begin");
  mp_writer->setCellAnnotation(
    "retention time of the max intensity in the peak");
  mp_writer->writeCell("rt_max");
  mp_writer->setCellAnnotation("right boundary of the peak");
  mp_writer->writeCell("rt_end");
  mp_writer->setCellAnnotation("Isotope");
  mp_writer->writeCell("isotope");
}

void
XicAreaSheet::writeHeadersAllData()
{
  mp_writer->writeLine();
  mp_writer->setCellAnnotation("ms run name");
  mp_writer->writeCell("msrun");
  mp_writer->setCellAnnotation("Isotope");
  mp_writer->writeCell("isotope");
  mp_writer->setCellAnnotation("retention time of point");
  mp_writer->writeCell("rt");
  mp_writer->setCellAnnotation("intensity of point");
  mp_writer->writeCell("intensity");
}


void
XicAreaSheet::writeAreaPeackdata(XicBox *xic_box)
{
  QSettings settings;
  bool xic_represented_only =
    settings.value("export/xic_only_represented", "false").toBool();

  if(xic_represented_only)
    {
      for(XicBoxNaturalIsotope isotope : xic_box->getNaturalIsotopList())
        {
          for(pappso::TracePeakCstSPtr trace : isotope.detected_peak_list)
            {
              if((trace->getLeftBoundary().x > xic_box->getRtRange().lower) &&
                 (trace->getRightBoundary().x < xic_box->getRtRange().upper))
                {
                  mp_writer->writeLine();
                  mp_writer->writeCell(
                    QFileInfo(xic_box->getMsRunSp()->getFileName()).fileName());
                  mp_writer->writeCell(trace->getArea());
                  mp_writer->writeCell(trace->getLeftBoundary().x);
                  mp_writer->writeCell(trace->getMaxXicElement().x);
                  mp_writer->writeCell(trace->getRightBoundary().x);
                  mp_writer->writeCell(int(
                    isotope.peptide_natural_isotope_sp->getIsotopeNumber()));
                }
            }
        }
    }
  else
    {
      for(XicBoxNaturalIsotope isotope : xic_box->getNaturalIsotopList())
        {
          for(pappso::TracePeakCstSPtr trace : isotope.detected_peak_list)
            {
              mp_writer->writeLine();
              mp_writer->writeCell(
                QFileInfo(xic_box->getMsRunSp()->getFileName()).fileName());
              mp_writer->writeCell(trace->getArea());
              mp_writer->writeCell(trace->getLeftBoundary().x);
              mp_writer->writeCell(trace->getMaxXicElement().x);
              mp_writer->writeCell(trace->getRightBoundary().x);
              mp_writer->writeCell(
                int(isotope.peptide_natural_isotope_sp->getIsotopeNumber()));
            }
        }
    }
}

void
XicAreaSheet::writeXicAllData(XicBox *xic_box)
{
  QSettings settings;
  bool xic_represented_only =
    settings.value("export/xic_only_represented", "false").toBool();

  if(xic_represented_only)
    {
      for(XicBoxNaturalIsotope isotope : xic_box->getNaturalIsotopList())
        {
          for(std::size_t i = 0; i < isotope.xic_sp->xValues().size(); i++)
            {
              if(isotope.xic_sp->xValues().at(i) >
                   xic_box->getRtRange().lower &&
                 isotope.xic_sp->xValues().at(i) < xic_box->getRtRange().upper)
                {
                  mp_writer->writeLine();
                  mp_writer->writeCell(
                    QFileInfo(xic_box->getMsRunSp()->getFileName()).fileName());
                  mp_writer->writeCell(int(
                    isotope.peptide_natural_isotope_sp->getIsotopeNumber()));
                  mp_writer->writeCell(isotope.xic_sp->xValues().at(i));
                  mp_writer->writeCell(isotope.xic_sp->yValues().at(i));
                }
            }
        }
    }
  else
    {
      for(XicBoxNaturalIsotope isotope : xic_box->getNaturalIsotopList())
        {
          for(std::size_t i = 0; i < isotope.xic_sp->xValues().size(); i++)
            {
              mp_writer->writeLine();
              mp_writer->writeCell(
                QFileInfo(xic_box->getMsRunSp()->getFileName()).fileName());
              mp_writer->writeCell(
                int(isotope.peptide_natural_isotope_sp->getIsotopeNumber()));
              mp_writer->writeCell(isotope.xic_sp->xValues().at(i));
              mp_writer->writeCell(isotope.xic_sp->yValues().at(i));
            }
        }
    }
}
