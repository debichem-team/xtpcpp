/**
 * \file output/ods/comparspecificspectrasheet.cpp
 * \date 1/5/2017
 * \author Olivier Langella
 * \brief ODS compar specific spectra sheet
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "comparspecificspectrasheet.h"


ComparSpecificSpectraSheet::ComparSpecificSpectraSheet(
  OdsExport *p_ods_export,
  CalcWriterInterface *p_writer,
  const Project *p_project,
  SubMonitor &sub_monitor)
  : ComparBaseSheet(p_ods_export, p_writer, p_project, sub_monitor)
{
  _title_sheet = "compar specific spectra";
}


void
ComparSpecificSpectraSheet::writeComparValue(
  const ProteinMatch *p_protein_match,
  ValidationState state,
  const MsRun *p_msrun,
  const Label *p_label)
{
  qDebug() << "ComparSpecificSpectraSheet::writeComparValue begin";
  _p_writer->writeCell(
    p_protein_match->getGroupingGroupSp().get()->countSpecificSampleScan(
      p_protein_match, state, p_msrun, p_label));
  qDebug() << "ComparSpecificSpectraSheet::writeComparValue end";
}


ComparSpecificSequenceSheet::ComparSpecificSequenceSheet(
  OdsExport *p_ods_export,
  CalcWriterInterface *p_writer,
  const Project *p_project,
  SubMonitor &sub_monitor)
  : ComparBaseSheet(p_ods_export, p_writer, p_project, sub_monitor)
{
  _title_sheet = "compar specific unique sequence";
}

void
ComparSpecificSequenceSheet::writeComparValue(
  const ProteinMatch *p_protein_match,
  ValidationState state,
  const MsRun *p_msrun,
  const Label *p_label)
{
  qDebug() << "ComparSpecificSequenceSheet::writeComparValue begin";
  _p_writer->writeCell(
    p_protein_match->getGroupingGroupSp().get()->countSpecificSequenceLi(
      p_protein_match, state, p_msrun, p_label));
  qDebug() << "ComparSpecificSequenceSheet::writeComparValue end";
}
