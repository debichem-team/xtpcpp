/**
 * \file output/ods/mcqrmetadasheet.cpp
 * \date 13/01/2021
 * \author Thomas
 * \brief ODS with Mcqr mandatory metadata template
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#include "mcqrmetadatasheet.h"

McqrMetadataSheet::McqrMetadataSheet(CalcWriterInterface *p_writer,
                                     std::vector<MsRunSp> msruns,
                                     McqrLoadDataMode method)
{
  qDebug();
  mp_writer     = p_writer;
  m_msrunSpList = msruns;
  m_method      = method;

  qDebug();
  mp_writer->writeSheet("metadata");
  qDebug();
  writeHeaders();
  writeMsRunUsed();
  qDebug();
}

void
McqrMetadataSheet::writeHeaders()
{
  qDebug();
  mp_writer->writeLine();
  mp_writer->writeCell("msrun");
  mp_writer->writeCell("msrunfile");
  if(m_method == McqrLoadDataMode::label || m_method == McqrLoadDataMode::both)
    {
      mp_writer->writeCell("label");
    }
  if(m_method == McqrLoadDataMode::fraction ||
     m_method == McqrLoadDataMode::both)
    {
      mp_writer->writeCell("fraction");
      mp_writer->writeCell("fraction_order");
      mp_writer->writeCell("track");
      mp_writer->writeCell("track_order");
    }
  qDebug();
}

void
McqrMetadataSheet::writeMsRunUsed()
{
  qDebug();
  for(MsRunSp msrun : m_msrunSpList)
    {
      mp_writer->writeLine();
      mp_writer->writeCell(msrun->getXmlId());
      mp_writer->writeCell(QFileInfo(msrun->getFileName()).baseName());
    }
  qDebug();
}
