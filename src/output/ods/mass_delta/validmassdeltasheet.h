/**
 * \file src/output/ods/mass_delta/validmassdeltasheet.h
 * \date 17/11/2020
 * \author Olivier Langella
 * \brief mass delta output
 */
/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella
 *<olivier.langella@universite-paris-saclay.fr>
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include "../odsexport.h"

class ValidMassDeltaSheet
{
  public:
  ValidMassDeltaSheet(OdsExport *p_ods_export,
                      CalcWriterInterface *p_writer,
                      const Project *p_project);

  private:
  OdsExport *_p_ods_export;
  const Project *_p_project;
  CalcWriterInterface *_p_writer;
};
