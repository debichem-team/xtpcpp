/**
 * \file output/ods/comparbasesheet.h
 * \date 2/5/2017
 * \author Olivier Langella
 * \brief ODS compar base sheet
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include "../../core/proteinmatch.h"
#include "odsexport.h"

class ComparBaseSheet
{
  public:
  ComparBaseSheet(OdsExport *p_ods_export,
                  CalcWriterInterface *p_writer,
                  const Project *p_project,
                  SubMonitor &sub_monitor);

  private:
  void writeIdentificationGroup(IdentificationGroup *p_ident);
  void writeHeaders(IdentificationGroup *p_ident);

  public:
  void writeSheet();

  protected:
  virtual void writeComparValue(const ProteinMatch *p_protein_match,
                                ValidationState state,
                                const MsRun *p_msrun,
                                const Label *p_label = nullptr) = 0;
  void writeProteinMatch(const ProteinMatch *p_protein_match);


  protected:
  OdsExport *_p_ods_export;
  const Project *_p_project;
  CalcWriterInterface *_p_writer;
  std::vector<MsRunSp> _msrun_list;
  QString _title_sheet;

  QString _first_cell_coordinate;

  IdentificationGroup *_p_current_identification_group;

  LabelingMethodSp _sp_labelling_method;
  std::vector<Label *> _label_list;
  SubMonitor &m_subMonitor;
};
