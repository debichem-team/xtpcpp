/**
 * \file /output/groupingsheet.cpp
 * \date 9/5/2017
 * \author Olivier Langella
 * \brief ODS grouping sheet
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "groupingsheet.h"


GroupingSheet::GroupingSheet(OdsExport *p_ods_export,
                             CalcWriterInterface *p_writer,
                             const Project *p_project)
  : _p_project(p_project)
{
  _p_writer     = p_writer;
  _p_ods_export = p_ods_export;
  p_writer->writeSheet("groups");


  OdsTableSettings table_settings;
  table_settings.setVerticalSplit(1);
  _p_writer->setCurrentOdsTableSettings(table_settings);

  writeHeaders();

  std::vector<IdentificationGroup *> identification_list =
    p_project->getIdentificationGroupList();
  for(IdentificationGroup *p_ident : identification_list)
    {
      // writeHeaders(p_ident);
      writeIdentificationGroup(p_ident);
    }
}

void
GroupingSheet::writeIdentificationGroup(IdentificationGroup *p_ident)
{
  _p_writer->writeLine();
  if(_p_project->getProjectMode() == ProjectMode::individual)
    {
      _p_writer->writeCell(
        p_ident->getMsRunSpList().at(0).get()->getSampleName());
    }
  _p_writer->writeCell(p_ident->countGroup());
  _p_writer->writeCell(p_ident->countSubGroup());
  _p_writer->writeCell(p_ident->countProteinMatch(ValidationState::grouped));
  _p_writer->writeCell(p_ident->countPeptideMass(ValidationState::grouped));
  _p_writer->writeCell(p_ident->countPeptideMatch(ValidationState::grouped));
  //_p_writer->writeCell(p_ident->countSequence(ValidationState::grouped));
  _p_writer->writeCell(
    p_ident->countPeptideMassSample(ValidationState::grouped));
  _p_writer->writeCellPercentage(
    p_ident->getProteinFdr(ValidationState::valid));
  _p_writer->writeCellPercentage(
    p_ident->getPeptideMassFdr(ValidationState::valid));
  _p_writer->writeCellPercentage(p_ident->getPsmFdr(ValidationState::valid));
}


void
GroupingSheet::writeHeaders()
{
  // groups, subgroups, proteins, psm, sequence, peptide/mass, pep FDR, prot FDR

  _p_writer->writeLine();
  if(_p_project->getProjectMode() == ProjectMode::individual)
    {
      _p_writer->writeCell("sample");
    }
  _p_writer->setCellAnnotation("number of groups");
  _p_writer->writeCell("groups");
  _p_writer->setCellAnnotation("number of subgroups");
  _p_writer->writeCell("subgroups");
  _p_writer->setCellAnnotation("number of grouped proteins");
  _p_writer->writeCell("proteins");
  _p_writer->setCellAnnotation(
    "number of distinct grouped peptides in the whole experiment");
  _p_writer->writeCell("peptides");
  _p_writer->setCellAnnotation(
    "number of peptide hits : each grouped peptide spectrum match given by the "
    "identification engine");
  _p_writer->writeCell("psm");
  //_p_writer->writeCell("sequences");
  _p_writer->setCellAnnotation(
    "number of unique combinations : grouped "
    "peptide sequence+modifications+sample name");
  _p_writer->writeCell("peptide/mass/sample");
  _p_writer->setCellAnnotation(
    "FDR at the protein level (number of decoy valid proteins / total number "
    "of valid proteins)");
  _p_writer->writeCell("prot FDR");
  _p_writer->setCellAnnotation(
    "FDR at the peptide level (number of decoy valid peptides / total number "
    "of valid peptides)");
  _p_writer->writeCell("pep FDR");
  _p_writer->setCellAnnotation(
    "FDR at the peptide spectrum match (peptide evidences for each association "
    "of a spectrum to a peptide, overall identification engines) level (number "
    "of decoy valid psm / total number "
    "of valid psm)");
  _p_writer->writeCell("psm FDR");
}
