/**
 * \file /output/ods/xicinfosheet.h
 * \date 09/12/2020
 * \author Thomas Renne
 * \brief ODS XIC info sheet
 */

/*******************************************************************************
 * Copyright (c) 2020 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include <odsstream/calcwriterinterface.h>
#include "../../core/project.h"
#include "../../gui/xic_view/xic_box/xicbox.h"

class XicInfoSheet
{
  public:
  XicInfoSheet(CalcWriterInterface *p_writer,
               ProjectSp project_sp,
               const std::vector<XicBox *> xic_boxs);

  private:
  void writeXicBoxData(XicBox *xic_box);
  void writeXicParamData(const MasschroqFileParameters & mcq_params);
  void writeHeaders();

  private:
  std::vector<XicBox *> mp_xicBoxList;
  CalcWriterInterface *mp_writer;
};
