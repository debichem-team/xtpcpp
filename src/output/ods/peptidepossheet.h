/**
 * \file output/ods/peptidepossheet.h
 * \date 30/4/2017
 * \author Olivier Langella
 * \brief ODS peptide pos sheet
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include "../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include "../../core/proteinmatch.h"
#include "odsexport.h"
#include "../../gui/peptide_list_view/peptidetablemodel.h"

class PeptidePosSheet
{
  public:
  PeptidePosSheet(OdsExport *p_ods_export,
                  CalcWriterInterface *p_writer,
                  const Project *p_project);

  private:
  void writeIdentificationGroup(IdentificationGroup *p_ident);
  void writeHeaders(IdentificationGroup *p_ident);
  void writeBestPeptideMatch(const ProteinMatch *p_protein_match,
                             const PeptideMatch &peptide_match);

  void writeCellHeader(PeptideListColumn column);

  private:
  OdsExport *_p_ods_export;
  const Project *_p_project;
  CalcWriterInterface *_p_writer;
  std::vector<size_t> m_sample_scan_list;
};
