/**
 * \file output/masschroqml.h
 * \date 7/4/2017
 * \author Olivier Langella
 * \brief MassChroQML writer
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QXmlStreamWriter>
#include <QFile>
#include <QString>
#include <pappsomspp/processing/detection/tracedetectionzivy.h>
#include "../core/project.h"
#include "../grouping/groupinggroup.h"
#include "../gui/xic_view/xic_widgets/zivywidget.h"
#include "../core/masschroq_run/masschroqfileparameters.h"

class MassChroQml
{
  public:
  MassChroQml(const QString &out_filename,
              const MasschroqFileParameters &params);
  ~MassChroQml();

  void write(ProjectSp sp_project);
  void close();

  private:
  void writeGroups();
  void writeProteinList();
  void writePeptideList();
  void writePeptideListInGroup(const GroupingGroup *p_group);
  void writeIsotopeLabelList();
  void writeAlignments();
  void writeQuantificationMethods();
  void writeQuantificationResults();
  void writeQuantificationTraces();
  void writeQuantify();
  void writeXicFilters();

  private:
  const MasschroqFileParameters &m_params;
  QFile *_output_file;
  QXmlStreamWriter *_output_stream;
  ProjectSp _sp_project;
  IdentificationGroup *_p_identification_group;
};
