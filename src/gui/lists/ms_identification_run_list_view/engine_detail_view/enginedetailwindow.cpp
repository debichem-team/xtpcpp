
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Thomas Renne <thomas.renne@u-psud.fr>. - initial API and
 *implementation
 ******************************************************************************/

#include "ui_engine_detail_view.h"
#include "enginedetailwindow.h"
#include "gui/widgets/engines_view/xtandemparamwidget.h"
#include "gui/widgets/engines_view/noengineparamwidget.h"
#include "../../../mainwindow.h"
//#include <QSettings>
#include <QCloseEvent>


EngineDetailWindow::EngineDetailWindow(ProjectWindow *parent)
  : QMainWindow(parent), ui(new Ui::EngineDetailView)
{
  m_project_window = parent;
  ui->setupUi(this);
  setWindowIcon(QIcon(":/xtpcpp_icon/resources/xtandempipeline_icon.svg"));
}


EngineDetailWindow::~EngineDetailWindow()
{
  delete m_preset_dialog;
}

void
EngineDetailWindow::setIdentificationEngineParam(
  IdentificationDataSourceSp *identificationEngine)
{
  m_identificationEngine = identificationEngine;
  updateDisplay();
}

void
EngineDetailWindow::doEditXtandemParameters()
{
  m_preset_dialog = new EditTandemPresetDialog(this);

  m_preset_dialog->newTandemParameters(m_xtandem_parameters);
  m_preset_dialog->show();
}


void
EngineDetailWindow::updateDisplay()
{
  QString params_file = m_identificationEngine->get()->getResourceName();

  ui->labelEngineName->setText(
    m_identificationEngine->get()->getIdentificationEngineName());
  ui->labelEngineVersion->setText(
    m_identificationEngine->get()->getIdentificationEngineVersion());

  TandemParametersFile param_file(params_file);
  if(param_file.exists())
    {
      m_xtandem_parameters = param_file.getTandemParameters();
      if(m_identificationEngine->get()->getIdentificationEngineName() ==
         "X!Tandem")
        {
          XtandemParamWidget *xtandem_view_widget;
          xtandem_view_widget = new XtandemParamWidget(this);
          ui->verticalLayout->insertWidget(1, xtandem_view_widget);

          xtandem_view_widget->setAutomaticXTandemParameters(
            m_xtandem_parameters);
          qDebug() << param_file.getAbsoluteFilePath();
          ui->labelPreset->setText(param_file.getMethodName());
          ui->actionedit_X_Tandem_parameters->setEnabled(true);
        }
      else
        {
          NoEngineParamWidget *no_engine_view_widget;
          QString message =
            m_identificationEngine->get()->getIdentificationEngineName() +
            " parameters view isn't implemented\nComing Soon";
          ui->labelPreset->setText("No preset file found");
          no_engine_view_widget = new NoEngineParamWidget(this, message);
          ui->verticalLayout->insertWidget(1, no_engine_view_widget);
        }
    }
  else
    {
      NoEngineParamWidget *no_engine_view_widget;
      QString message =
        params_file + "\nwasn't found !!\n Please check if the file exist";

      no_engine_view_widget = new NoEngineParamWidget(this, message);
      ui->verticalLayout->insertWidget(1, no_engine_view_widget);
    }
}


void
EngineDetailWindow::closeEvent(QCloseEvent *event)
{
  QWidget *temp_widget = ui->verticalLayout->itemAt(1)->widget();
  ui->verticalLayout->removeWidget(temp_widget);
  delete temp_widget;

  m_project_window->popEngineDetailView();
  event->accept();
}
