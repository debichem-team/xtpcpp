/**
 * \file src/gui/xic_view/xic_widgets/zivydialog.h
 * \date 30/5/2018
 * \author Olivier Langella
 * \brief dialog window to edit Zivy quantification method
 */
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QDialogButtonBox>
#include <QDialog>
#include "zivywidget.h"
#include "xicexportparam.h"
#include "../../../core/masschroq_run/masschroqfileparameters.h"

class ZivyDialog : public QDialog
{
  Q_OBJECT

  public:
  explicit ZivyDialog(QWidget *parent = 0);
  virtual ~ZivyDialog();


  void setMasschroqFileParametersSp(const MasschroqFileParametersSp &params);
  void updateProjectMasschroqFileParameters() const;

  void setXicExportParams();
  void saveXicExportSettings();

  private:
  QLabel *_p_display_filters;
  ZivyWidget *_p_zivy_widget;
  XicExportWidget *mp_xicExportWidget;
  QDialogButtonBox *_p_button_box;
  MasschroqFileParametersSp msp_MasschroqFileParametersSp;
};
