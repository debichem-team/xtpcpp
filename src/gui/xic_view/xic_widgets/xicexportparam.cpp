/**
 * \file src/gui/xic_view/xic_widgets.cpp
 * \date 08/12/2020
 * \author Thomas Renne
 * \brief XIC export parameter widget
 */
/*******************************************************************************
 * Copyright (c) 2020 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#include "xicexportparam.h"
#include "ui_xic_export_param.h"
#include <QSettings>

XicExportWidget::XicExportWidget(QWidget *parent)
  : QWidget(parent), ui(new Ui::XicExportWidget)
{
  ui->setupUi(this);
}

XicExportWidget::~XicExportWidget()
{
}

void
XicExportWidget::setXicExportParams()
{
  qDebug() << "begin";
  QSettings settings;

  // Set combobox position based on settings
  QString xic_files = settings.value("export/xic_files", "both").toString();
  if(xic_files == "area")
    {
      ui->data_export_combobox->setCurrentIndex(1);
    }
  else if(xic_files == "all")
    {
      ui->data_export_combobox->setCurrentIndex(0);
    }
  else // xic_files == "both"
    {
      ui->data_export_combobox->setCurrentIndex(2);
    }

  // Set radio button position
  bool only_represented =
    settings.value("export/xic_only_represented", "false").toBool();
  if(only_represented)
    {
      qDebug() << "ok";
      ui->represented_button->setChecked(true);
    }
  else // only_represented == "no"
    {
      qDebug() << "pas ok";
      ui->all_data_button->setChecked(true);
    }
}

void
XicExportWidget::saveXicExportSettings()
{
  QSettings settings;
  if(ui->data_export_combobox->currentIndex() == 0)
    {
      settings.setValue("export/xic_files", "all");
    }
  else if(ui->data_export_combobox->currentIndex() == 1)
    {
      settings.setValue("export/xic_files", "area");
    }
  else // currentIndex == 2
    {
      settings.setValue("export/xic_files", "both");
    }
  settings.setValue("export/xic_only_represented",
                    ui->represented_button->isChecked());
}
