/**
 * \file gui/ptm_island_list_window/ptmislandlistwindow.h
 * \date 30/5/2017
 * \author Olivier Langella
 * \brief display all ptm islands
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QMainWindow>
#include <QLabel>
#include <QAction>
#include "ptmislandtablemodel.h"
#include "ptmislandproxymodel.h"
#include "../../core/identificationgroup.h"

class ProjectWindow;
class PtmPeptideListWindow;

// http://doc.qt.io/qt-4.8/qt-itemviews-chart-mainwindow-cpp.html
namespace Ui
{
class PtmIslandListWindow;
}

class PtmIslandListWindow;
class PtmIslandListQActionColumn : public QAction
{
  Q_OBJECT
  public:
  explicit PtmIslandListQActionColumn(PtmIslandListWindow *parent,
                                      PtmIslandListColumn column);
  ~PtmIslandListQActionColumn();


  public slots:
  void doToggled(bool toggled);

  private:
  PtmIslandListWindow *mp_ptmIslandWindow;
  PtmIslandListColumn m_column;
};

class PtmIslandListWindow : public QMainWindow
{
  Q_OBJECT
  friend PtmIslandProxyModel;

  public:
  explicit PtmIslandListWindow(ProjectWindow *parent = 0);
  ~PtmIslandListWindow();
  void setIdentificationGroup(IdentificationGroup *p_identification_group);
  void resizeColumnsToContents();
  const IdentificationGroup *getIdentificationGroup() const;
  ProjectWindow *getProjectWindowP();
  void setPtmIslandListColumnDisplay(PtmIslandListColumn column, bool toggled);
  bool getPtmIslandListColumnDisplay(PtmIslandListColumn column) const;
  void setColumnMenuList();
  void edited();

  public slots:
  void
  doIdentificationPtmGroupGrouped(IdentificationGroup *p_identification_group);
  void
  doIdentificationGroupGrouped(IdentificationGroup *p_identification_group);

  protected slots:
  void updateStatusBar();
  void doPtmSearchEdit(QString ptm_search_string);
  void doSearchOn(QString search_on);
  void doNotCheckedHide(bool hide);


  signals:
  void ptmIslandDataChanged();

  protected:
  void askViewPtmPeptideList(PtmIsland *ptm_island);
  void askProteinDetailView(ProteinMatch *p_protein_match);

  private:
  void connectNewPtmPeptideListWindow();

  private:
  IdentificationGroup *_p_identification_group = nullptr;
  Ui::PtmIslandListWindow *ui;
  PtmIslandTableModel *_ptm_table_model_p = nullptr;
  PtmIslandProxyModel *_ptm_proxy_model_p = nullptr;
  ProjectWindow *_project_window;

  PtmPeptideListWindow *_p_current_ptm_peptide_list_window;
  std::vector<PtmPeptideListWindow *> _ptm_peptide_list_window_collection;

  QLabel *_statusbar_displayed_label;
  QLabel *_statusbar_ptm_islands_label;
};
