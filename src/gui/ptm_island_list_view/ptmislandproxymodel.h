/**
 * \file gui/ptm_island_list_window/ptmislandproxymodel.h
 * \date 30/5/2017
 * \author Olivier Langella
 * \brief display all ptm islands
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include "ptmislandtablemodel.h"


class PtmIslandListWindow;
class PtmIslandTableModel;

class PtmIslandProxyModel : public QSortFilterProxyModel
{
  Q_OBJECT
  public:
  PtmIslandProxyModel(PtmIslandListWindow *p_ptm_island_list_window,
                      PtmIslandTableModel *ptm_table_model_p);
  ~PtmIslandProxyModel();
  bool filterAcceptsColumn(int source_column,
                           const QModelIndex &source_parent) const override;
  bool filterAcceptsRow(int source_row,
                        const QModelIndex &source_parent) const override;

  void setSearchOn(QString search_on);
  void hideNotChecked(bool hide);
  void setPtmSearchString(QString ptm_search_string);
  void setPtmIslandListColumnDisplay(PtmIslandListColumn column, bool toggled);
  bool getPtmIslandListColumnDisplay(PtmIslandListColumn column) const;

  public slots:
  void onTableClicked(const QModelIndex &index);

  private:
  PtmIslandListWindow *_p_ptm_island_list_window;
  PtmIslandTableModel *_p_ptm_island_table_model;
  std::vector<bool> m_column_display;
  bool m_hideNotChecked = false;
  QString _search_on;
  QString _ptm_search_string;
};
