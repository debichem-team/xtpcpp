
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include <QMainWindow>
#include <QTextDocument>
#include <QThread>
#include <QLabel>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/massspectrum/massspectrum.h>
#include <pappsomspp/psm/peakionisotopematch.h>
#include "../../core/peptidematch.h"
#include "../../core/identificationgroup.h"

class ProjectWindow;

namespace Ui
{
class PeptideDetailView;
}


class SpectrumSpLoaderThread : public QObject
{
  Q_OBJECT
  public:
  public slots:
  void doLoadSpectrumSp(PeptideEvidence *p_peptide_evidence);

  signals:
  void spectrumSpReady(pappso::MassSpectrumCstSPtr spectrum_sp,
                       QString error,
                       QString fatal_error);

  protected:
  void closeEvent(QCloseEvent *event);
};


class PeptideWindow : public QMainWindow
{
  Q_OBJECT

  public:
  explicit PeptideWindow(ProjectWindow *parent = 0);
  ~PeptideWindow();
  void setPeptideEvidence(PeptideEvidence *p_peptide_evidence);

  public slots:
  void
  doIdentificationGroupGrouped(IdentificationGroup *p_identification_group);
  void setMz(double);
  void setPeak(pappso::DataPointCstSPtr p_peak_match);
  void setIon(pappso::PeakIonIsotopeMatchCstSPtr ion);
  void doProjectNameChanged(QString name);

  signals:
  void loadSpectrumSp(PeptideEvidence *p_peptide_evidence);

  protected slots:
  void doSpectrumSpReady(pappso::MassSpectrumCstSPtr spectrum_sp,
                         QString error,
                         QString fatal_error);
  void doMsmsPrecisionChanged(pappso::PrecisionPtr);
  void doSaveSvg();
  void chooseDefaultMzDataDir();
  void openInPeptideViewer();
  void openInXicViewer();

  protected:
  void updateDisplay();

  private:
  void updateIonMobilityDisplay();
  bool checkPeptideViewerWorking();

  private:
  QThread _spectrum_loader_thread;
  QLabel *_mz_label;
  QLabel *_peak_label;
  QLabel *_ion_label;
  QLabel *mp_status_label;
  QMovie *mp_movie;

  Ui::PeptideDetailView *ui;
  ProjectWindow *_p_project_window;
  PeptideEvidence *_p_peptide_evidence = nullptr;
  pappso::PrecisionPtr _p_precision;

  bool _spectrum_is_ready   = false;
  bool m_precursorHighlight = false;
};
