
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "modificationbrowser.h"

#include "ui_uimodificationbrowser.h"
#include <QDebug>

ModificationBrowser::ModificationBrowser(QWidget *parent)
  : QMainWindow(parent), ui(new Ui::ModificationBrowser)
{
  qDebug();
  ui->setupUi(this);

  // param.setFilterCrossSamplePeptideNumber(settings.value("automatic_filter/cross_sample",
  // "true").toBool());
}

ModificationBrowser::~ModificationBrowser()
{
  qDebug();
  delete ui;

  qDebug();
}

void
ModificationBrowser::setMzTarget(double target_mz)
{
  qDebug();
  ui->oboChooserWidget->setMzTarget(target_mz);
}

void
ModificationBrowser::setPrecision(pappso::PrecisionPtr precision)
{
  qDebug();
  ui->oboChooserWidget->setPrecision(precision);
}
