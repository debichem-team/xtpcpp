
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "editlabelmethods.h"
#include "../../project_view/projectwindow.h"
#include "ui_edit_label_methods.h"
#include <QFile>
#include <QDomDocument>
#include <QMessageBox>

EditLabelMethods::EditLabelMethods(ProjectWindow *parent)
  : QDialog(), ui(new Ui::EditLabelMethodView)
{
  ui->setupUi(this);
  _project_window        = parent;
  _p_label_method_str_li = new QStandardItemModel();
  ui->method_list_view->setModel(_p_label_method_str_li);
  this->setModal(true);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(ui->method_list_view,
          &QListView::clicked,
          this,
          &EditLabelMethods::ItemClicked);
#else

  connect(ui->method_list_view,
          SIGNAL(clicked(const QModelIndex)),
          this,
          SLOT(ItemClicked(QModelIndex)));


#endif
}
EditLabelMethods::~EditLabelMethods()
{

  qDebug() << "EditLabelMethods::~EditLabelMethods";
  delete ui;
  qDebug() << "EditLabelMethods::~EditLabelMethods end";
}
void
EditLabelMethods::setProjectSp(ProjectSp project_sp)
{

  _project_sp = project_sp;
  _p_label_method_str_li->removeRows(0, _p_label_method_str_li->rowCount());
  ui->ok_button->setDisabled(true);

  QDomDocument *dom = new QDomDocument("labeling_methods");
  QFile xml_doc(":/labeling/resources/catalog_label.xml");
  if(!xml_doc.open(QIODevice::ReadOnly))
    {
      // error
      QMessageBox::warning(
        this, tr("error"), tr("error opening catalog_label resource file"));
      return;
    }
  if(!dom->setContent(&xml_doc))
    {
      xml_doc.close();
      QMessageBox::warning(
        this, tr("error"), tr("error setting catalog_label xml content"));
      return;
    }

  QDomNode child = dom->documentElement().firstChild();
  while(!child.isNull())
    {
      if(child.toElement().tagName() == "isotope_label_list")
        {
          QStandardItem *item;
          item = new QStandardItem(
            QString("%1").arg(child.toElement().attribute("id")));
          item->setEditable(false);
          _p_label_method_str_li->appendRow(item);
          item->setData(
            QVariant(QString("%1").arg(child.toElement().attribute("id"))),
            Qt::UserRole);
        }
      child = child.nextSibling();
    }


  xml_doc.close();
  delete dom;
}

void
EditLabelMethods::ItemClicked(QModelIndex index)
{
  qDebug() << "begin" << index.data().toString();
  ui->ok_button->setDisabled(false);

  LabelingMethod method(index.data().toString());

  _sp_labeling_method = method.makeLabelingMethodSp();
}


LabelingMethodSp
EditLabelMethods::getLabelingMethodSp() const
{
  //_project_sp.get()->setLabelingMethodSp(_sp_labeling_method);
  return _sp_labeling_method;
}
