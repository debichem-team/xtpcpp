
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QMainWindow>
#include <QStandardItemModel>
#include "../../choose_modification_dialog/choosemodificationdialog.h"
#include "../../../core/project.h"

class ProjectWindow;

// http://doc.qt.io/qt-4.8/qt-itemviews-chart-mainwindow-cpp.html
namespace Ui
{
class EditModificationView;
}

class EditModifications : public QMainWindow
{
  Q_OBJECT
  public:
  explicit EditModifications(ProjectWindow *parent = 0);
  ~EditModifications();

  void setProjectSp(ProjectSp project_sp);

  signals:
  void projectStatusChanged();

  public slots:
  void ItemClicked(QModelIndex index);
  void ItemDoubleClicked(QModelIndex index);
  private slots:
  void doAcceptedBrowseDialog();
  void doActionReplace();
  void doActionBrowse();

  private:
  void setSelectedModification(pappso::AaModificationP modification);
  void setReplaceModification(pappso::AaModificationP modification);

  private:
  Ui::EditModificationView *ui;
  ProjectWindow *_project_window;
  ProjectSp _project_sp;
  ChooseModificationDialog *_p_browse_modification_dialog;
  QStandardItemModel *_p_modification_str_li = nullptr;
  pappso::AaModificationP _selected_modification;
  pappso::AaModificationP _replace_modification;
};
