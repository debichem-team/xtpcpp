/**
 * \file gui/ptm_peptide_list_window/ptmpeptidelistwindow.h
 * \date 10/6/2017
 * \author Olivier Langella
 * \brief display all peptides from one ptm island
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PTMPEPTIDELISTWINDOW_H
#define PTMPEPTIDELISTWINDOW_H

#include <QMainWindow>
#include "../../grouping/ptm/ptmisland.h"
#include "ptmpeptidetablemodel.h"
#include "ptmpeptidetableproxymodel.h"

class PtmIslandListWindow;

namespace Ui
{
class PtmPeptideListWindow;
}


class PtmPeptideListWindow : public QMainWindow
{
  Q_OBJECT
  public:
  explicit PtmPeptideListWindow(PtmIslandListWindow *parent = 0);
  ~PtmPeptideListWindow();
  void setPtmIsland(PtmIsland *p_ptm_island);
  void askPeptideDetailView(PeptideEvidence *p_peptide_evidence);

  signals:
  void requestPeptideDetailView(PeptideEvidence *p_peptide_evidence);
  void ptmPeptideChanged();

  public slots:
  void doShowPtmPeptides();

  private:
  PtmIsland *_p_ptm_island = nullptr;
  Ui::PtmPeptideListWindow *ui;
  PtmPeptideTableModel *_ptm_table_model_p      = nullptr;
  PtmPeptideTableProxyModel *_ptm_proxy_model_p = nullptr;
  PtmIslandListWindow *_p_ptm_island_list_window;
  QMenu *_p_context_menu = nullptr;
};

#endif // PTMPEPTIDELISTWINDOW_H
