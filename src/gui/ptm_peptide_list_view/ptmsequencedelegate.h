/**
 * \file gui/ptm_peptide_list_window/ptmsequencedelegate.h
 * \date 28/7/2017
 * \author Olivier Langella
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PTMSEQUENCEDELEGATE_H
#define PTMSEQUENCEDELEGATE_H

#include <QStyledItemDelegate>
class PtmIslandListWindow;

class PtmSequenceDelegate : public QStyledItemDelegate
{
  Q_OBJECT

  public:
  PtmSequenceDelegate(PtmIslandListWindow *p_ptm_island_list_window,
                      QWidget *parent = 0);

  void paint(QPainter *painter,
             const QStyleOptionViewItem &option,
             const QModelIndex &index) const override;
  // QSize sizeHint(const QStyleOptionViewItem &option,
  //              const QModelIndex &index) const override;
  // QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
  //                      const QModelIndex &index) const override;
  // void setEditorData(QWidget *editor, const QModelIndex &index) const
  // override; void setModelData(QWidget *editor, QAbstractItemModel *model,
  //                  const QModelIndex &index) const override;
  private:
  PtmIslandListWindow *_p_ptm_island_list_window;
};
#endif // PTMSEQUENCEDELEGATE_H
