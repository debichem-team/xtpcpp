

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr>. - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QMainWindow>
#include <QAction>
#include <QMenu>
#include <QModelIndex>

#include "core/alignmentgroup.h"
#include <core/project.h>

class MsIdentificationListWindow;
class AlignmentGroupsQMenu : public QMenu
{
  Q_OBJECT
  public:
  explicit AlignmentGroupsQMenu(MsIdentificationListWindow *parent,
                                Project *project);
  ~AlignmentGroupsQMenu();

  void createGroupsSubMenu();
  void disableActionsFollowingSelection();

  private:
  int getMsRunAlignmentGroupPosition(QString new_name);
  void checkGroupsOnSelectedIndexes();
  void createNewAlignmentGroup();
  void handleMsRunReference(bool new_reference);
  public slots:
  void actionTriggered(QAction *action);

  private:
  MsIdentificationListWindow *mp_msIdListWindow;
  std::vector<MsRunAlignmentGroupSp> m_msrunAlignmentGroupList;
  std::vector<bool> m_selectedIndexesChecks;
  QMenu *mp_subMenuGroup;
  Project *mp_project;

  QAction *mp_addReference;
  QAction *mp_removeReference;
  QAction *mp_ungroup;
  QAction *mp_newGroup;
};
