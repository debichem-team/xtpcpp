# Lanceur version sds 0.5.0

want.mcqr.version <- "0.6.1"
want.mcqr.url <- "https://forgemia.inra.fr/pappso/mcqr/uploads/38a87af73d7ed179b0387c7e241510bf/MCQR_0.6.1.tar.gz"

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------- TABLE OF CONTENT ---------------------------------------------------------------------------------------------

# 1. Installing MCQR

# 2. Analyzing spectral counting (SC) data
#   2.1. Data and metadata loading
#   2.2. Checking data quality
#     2.2.1  Checking injections
#     2.2.2. Reconstituting samples from fractions
#     2.2.3  Checking the experiment
#     2.2.4. Filtering dubious data	
#   2.3. Filtering proteins
#   2.4. Overview of protein abundances
#   2.5. Analyzing protein abundance variations

# 3. Analyzing quantification data obtained from extracted ion chromatogram (XIC)
# 	3.1 Data and metadata loading
# 	3.2. Checking data quality
# 		3.2.1. Checking chromatography
# 		3.2.2. Checking injections
# 	3.3. Analyzing intensity data (quantitative variations)
# 		3.3.1. Filtering dubious data
# 		3.3.2. Reconstituting samples from fractions
# 		3.3.3. Normalizing peptide-mz intensities to take into account possible global quantitative variations between LC-MS runs
# 		3.3.4. Filtering for shared, unreproducible or uncorrelated peptides-mz
# 		3.3.5. Imputing missing peptide-mz intensities
# 		3.3.6. Computing protein abundances
# 		3.3.7. Imputing missing protein abundances
# 		3.3.8. Overview of the protein abundance data
# 		3.3.9. Analyzing protein abundance variations
# 		3.3.0. Exporting results in '/path/to/my_working_directory'
# 	3.4. Analyzing peak counting data (semi-quantitative variations)
# 		3.4.1. Counting 'peaks'
# 		3.4.2. Analyzing protein abundance variations
# 		3.4.3. Exporting results in '/path/to/my_working_directory'

# 4. Exporting a synthesis

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------




#**************************************************************************************************************************************************
#**************************************************************************************************************************************************
#******************************************************   1. Installing MCQR
#**************************************************************************************************************************************************
#**************************************************************************************************************************************************

# deb: libudunits2-dev libgdal-dev (Debian, Ubuntu, ...)
# rpm: udunits2-devel (Fedora, EPEL, ...)
# brew: udunits (OSX)


message("MCQRInfo: Check package installation")
# R base packages
all.packages = c('zoo', 'mvtnorm', 'multcomp', 'bitops', 'caTools', 'gplots', 'gtools', 'gdata', 'chron', 'stringi', 'stringr', 'reshape2', 'data.table', 'readODS', 'vcd', 'lme4', 'car', 'robustbase', 'colorspace', 'VIM', 'ade4', 'clValid', 'agricolae', 'ggplot2', 'gridExtra', 'Hmisc', 'RColorBrewer', 'plotly', 'plyr', 'dplyr', 'BiocManager', 'made4', 'ggVennDiagram', 'svglite')
#bioc.packages = c('made4')


# install all packages
packages.to.install <- setdiff(all.packages, rownames(installed.packages()))
for (pkg in packages.to.install) {
  cat("MCQRInfo: Downloading and installing", pkg)
#  if (pkg %in% bioc.packages){
#    library(BiocManager)
#    BiocManager::install(pkg)
#  }else{
    install.packages(pkg, quiet = TRUE, repos = "http://cran.us.r-project.org")
#  }
}

# Check if all the mandatory packages are installed
if(length(setdiff(all.packages, rownames(installed.packages()))) == 0){
  message("MCQRInfo: All packages are installed")
}else{
message("MCQRInfo: Some packages can't be installed :")
cat(setdiff(all.packages, rownames(installed.packages())))
}

# Check if MCQR is installed
is.installed <- function(mypkg){
    is.element(mypkg, installed.packages()[,1])
} 

# check if package "hydroGOF" is installed
if (!is.installed("MCQR")){
    install.packages(want.mcqr.url, repo=NULL, type="source")
}
if(packageVersion("MCQR") < want.mcqr.version) {
    install.packages(want.mcqr.url, repo=NULL, type="source")
}
