/**
 * \file src/input/tandem/tandemparamparser.cpp
 * \date 22/01/2022
 * \author Olivier Langella
 * \brief reads tandem xml parameter files to parse various informations
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "tandemparamparser.h"

TandemParamParser::TandemParamParser()
{
}

TandemParamParser::TandemParamParser(const TandemParamParser &other)
{
  m_tandemParameters = other.m_tandemParameters;
}

TandemParamParser::~TandemParamParser()
{
}

void
TandemParamParser::readStream()
{
  m_methodName    = "";
  m_paramFileType = ParamFileType::empty;
  m_tandemParameters.clear();
  m_tandemParameters.setParamLabelValue("output, xsl path", "tandem-style.xsl");
  if(m_qxmlStreamReader.readNextStartElement())
    {
      if(m_qxmlStreamReader.name() == "bioml")
        {
          //<note type="input" label="output, histogram column width">30</note>
          qDebug();
          while(m_qxmlStreamReader.readNextStartElement())
            {
              qDebug() << m_qxmlStreamReader.name();

              if(m_qxmlStreamReader.name() == "group")
                {
                  while(m_qxmlStreamReader.readNextStartElement())
                    {
                      qDebug() << m_qxmlStreamReader.name();
                      readNote();
                    }
                }
              else
                {
                  readNote();
                }
            }
        }
      else
        {
          m_paramFileType = ParamFileType::not_xtandem;
          m_qxmlStreamReader.raiseError(
            QObject::tr("Not an X!Tandem parameter file (no bioml)"));
          m_qxmlStreamReader.skipCurrentElement();
        }
    }

  qDebug();
}

void
TandemParamParser::readNote()
{
  qDebug() << m_qxmlStreamReader.name();
  if(m_qxmlStreamReader.name() == "note")
    {
      // read_note();
      QString type  = m_qxmlStreamReader.attributes().value("type").toString();
      QString label = m_qxmlStreamReader.attributes().value("label").toString();


      qDebug() << m_qxmlStreamReader.name() << " type=" << type
               << " label=" << label;

      if(type == "input")
        {

          QString value = m_qxmlStreamReader.readElementText().simplified();

          if(label == "list path, default parameters")
            {
              m_methodName    = QFileInfo(value).baseName();
              m_paramFileType = ParamFileType::result;
            }
          else
            {
              if(m_paramFileType == ParamFileType::empty)
                {
                  m_paramFileType = ParamFileType::preset;
                }
              m_tandemParameters.setParamLabelValue(label, value);
            }
        }
      else
        {
          m_qxmlStreamReader.skipCurrentElement();
        }
    }
  else
    { // not a note
      m_qxmlStreamReader.skipCurrentElement();
    }
  qDebug();
}

const TandemParameters &
TandemParamParser::getTandemParameters() const
{
  return m_tandemParameters;
}

const ParamFileType &
TandemParamParser::getParamFileType() const
{
  return m_paramFileType;
}

const QString &
TandemParamParser::getMethodName() const
{
  return m_methodName;
}
