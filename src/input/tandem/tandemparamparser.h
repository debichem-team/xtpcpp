/**
 * \file src/input/tandem/tandemparamparser.h
 * \date 22/01/2022
 * \author Olivier Langella
 * \brief reads tandem xml parameter files to parse various informations
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include "../../core/tandem_run/tandemparameters.h"
#include <QFileInfo>


enum class ParamFileType : std::int8_t
{
  preset      = 0, ///< Preset file type
  result      = 1, ///< Result file type
  not_xtandem = 2, ///< Not XTandem file type
  no_file     = 3, ///< No parameters file found
  empty       = 4
};

/**
 * @todo write docs
 */
class TandemParamParser : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   */
  TandemParamParser();

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  TandemParamParser(const TandemParamParser &other);

  /**
   * Destructor
   */
  ~TandemParamParser();

  const TandemParameters &getTandemParameters() const;

  const ParamFileType &getParamFileType() const;

  const QString &getMethodName() const;


  protected:
  virtual void readStream() override;

  private:
  void readNote();


  private:
  TandemParameters m_tandemParameters;
  ParamFileType m_paramFileType;
  QString m_methodName;
};
