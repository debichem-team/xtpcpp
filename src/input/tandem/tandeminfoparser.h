/**
 * \file src/input/tandem/tandeminfoparser.h
 * \date 23/12/2021
 * \author Olivier Langella
 * \brief reads tandem xml result files to parse various informations
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <QFile>
#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include <pappsomspp/types.h>

/**
 * @todo write docs
 */
class TandemInfoParser : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  TandemInfoParser();

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  TandemInfoParser(const TandemInfoParser &other);

  /**
   * Destructor
   */
  ~TandemInfoParser();


  const QString &getSpectraDataLocation() const;

  std::size_t getModelCount() const;

  pappso::MzFormat getMzFormat() const;

  protected:
  virtual void readStream() override;

  private:
  QString m_spectrum_path;
  std::size_t m_modelCount = 0;

  pappso::MzFormat m_mzFormat = pappso::MzFormat::unknown;
};
