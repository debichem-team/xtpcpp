/**
 * \file input/xtpxpipsaxhandler.h
 * \date 17/11/2017
 * \author Olivier Langella
 * \brief parse new xpip files from xtpcpp
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QXmlDefaultHandler>
#include <pappsomspp/pappsoexception.h>
#include "../core/proteinxtp.h"
#include "../core/peptidextp.h"
#include <pappsomspp/amino_acid/aamodification.h>
#include "../core/project.h"
#include "../core/proteinmatch.h"
#include "../utils/workmonitor.h"

class XtpXpipSaxHandler : public QXmlDefaultHandler
{
  public:
  XtpXpipSaxHandler(pappso::UiMonitorInterface *p_monitor, Project *p_project);
  ~XtpXpipSaxHandler();

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes);

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName);

  bool startDocument();

  bool endDocument();

  bool characters(const QString &str);

  bool fatalError(const QXmlParseException &exception);
  bool error(const QXmlParseException &exception);

  QString errorString() const;
  bool isXtpXpip() const;


  private:
  bool startElement_filter_params(QXmlAttributes attributes);
  bool startElement_description(QXmlAttributes attributes);
  bool startElement_fasta_file(QXmlAttributes attributes);
  bool startElement_protein_match(QXmlAttributes attributes);
  bool startElement_peptide_match(QXmlAttributes attributes);
  bool startElement_peptide(QXmlAttributes attributes);
  bool startElement_peptide_evidence(QXmlAttributes attributes);
  bool startElement_peptide_evidence_list(QXmlAttributes attributes);
  bool startElement_protein(QXmlAttributes attributes);
  bool startElement_msrun(QXmlAttributes attributes);
  bool startElement_alignmentGroup(QXmlAttributes attributes);
  bool startElement_msrunInAlignmentGroup(QXmlAttributes attributes);
  bool startElement_alignment_param(QXmlAttributes attributes);
  bool startElement_quantification_param(QXmlAttributes attributes);
  bool startElement_masschroq_param(QXmlAttributes attributes);
  bool startElement_identification_source(QXmlAttributes attributes);
  bool startElement_identification_group(QXmlAttributes attributes);

  bool startElement_modification(QXmlAttributes attributes);
  bool startElement_mod(QXmlAttributes attributes);
  bool startElement_param(QXmlAttributes attributes);
  bool startElement_stat(QXmlAttributes attributes);
  bool startElement_counts(QXmlAttributes attributes);
  bool startElement_label_method(QXmlAttributes attributes);
  bool startElement_contaminants(QXmlAttributes attributes);
  bool startElement_decoys(QXmlAttributes attributes);

  // bool endElement_identification();
  bool endElement_sequence();
  bool endElement_msrun();
  bool endElement_alignmentGroup();
  bool endElement_protein();
  bool endElement_peptide();
  bool endElement_protein_match();
  bool endElement_peptide_evidence();
  bool endElement_peptide_evidence_list();
  bool endElement_identification_group();
  bool endElement_identification_source();

  private:
  pappso::UiMonitorInterface *_p_monitor;
  std::vector<QString> _tag_stack;
  QString _errorStr;
  QString _current_text;
  AutomaticFilterParameters _automatic_filter_parameters;

  Project *_p_project;
  ProteinMatch *_p_protein_match;
  PeptideEvidence *_p_peptide_evidence;
  IdentificationGroup *_current_identification_group_p;

  std::map<QString, pappso::AaModificationP> _map_modifs;
  std::map<QString, FastaFileSp> _map_fasta_files;
  std::map<QString, ProteinXtpSp> _map_proteins;
  std::map<QString, MsRunSp> _map_msruns;
  std::map<QString, IdentificationDataSourceSp> _map_ident_sources;
  std::map<QString, PeptideEvidenceSp> _map_peptide_evidences;
  PeptideEvidenceSp _sp_current_peptide_evidence;
  ProteinXtpSp _sp_current_protein;
  PeptideXtpSp _current_peptide_sp;
  IdentificationDataSourceSp _sp_current_identification_source;
  LabelingMethodSp _sp_labeling_method;
  MsRunSp _sp_msrun;
  MsRunAlignmentGroupSp msp_alignment_group;
  std::map<QString, PeptideXtpSp> _map_peptides;
  bool _is_xtpcpp_xpip = false;
  QString _current_id;


  std::size_t _count_proteins          = 0;
  std::size_t _count_peptides          = 0;
  std::size_t _count_peptide_evidences = 0;
  std::size_t _count_protein_matches   = 0;
  std::size_t _count_total             = 0;
  std::size_t _total_proteins;
  std::size_t _total_peptides;
  std::size_t _total_peptide_evidences;
  std::size_t _total_protein_matches;
  std::size_t _total;
};
