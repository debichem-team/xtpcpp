/**
 * \file input/metadataodshandler.cpp
 * \date 13/01/2021
 * \author Thomas Renne
 * \brief set MCQR params and run
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#include "metadataodshandler.h"
#include <QDebug>

MetadataOdsHandler::MetadataOdsHandler(ProjectSp project)
{
  m_rowNumber = 0;
  msp_project = project;
}

MetadataOdsHandler::~MetadataOdsHandler()
{
}

void
MetadataOdsHandler::startSheet(const QString &sheet_name)
{
  qDebug() << sheet_name;
  msp_project->getMcqrExperimentSp()
    .get()
    ->clearMetadataInfoAndColumnsVectors();
}

void
MetadataOdsHandler::startLine()
{
  m_colNumber         = 0;
  mp_mcqrMetadataLine = new McqrMetadata();
}

void
MetadataOdsHandler::setCell(const OdsCell &cell)
{
  if(!cell.toString().isEmpty())
    {
      if(m_rowNumber == 0)
        {
          msp_project->getMcqrExperimentSp().get()->addColumnName(
            cell.toString());
          m_cols.push_back(cell.toString());
        }
      else
        {
          if(m_colNumber == 0)
            {
              mp_mcqrMetadataLine->m_msrunId = cell.toString();
            }
          else if(m_colNumber == 1)
            {
              mp_mcqrMetadataLine->m_msrunFile = cell.toString();
            }
          else
            {
              if(cell.isDouble())
                {
                  mp_mcqrMetadataLine->m_otherData.insert(
                    std::pair<QString, QVariant>(m_cols.at(m_colNumber),
                                                 cell.getDoubleValue()));
                }
              else if(cell.isString())
                {
                  mp_mcqrMetadataLine->m_otherData.insert(
                    std::pair<QString, QVariant>(m_cols.at(m_colNumber),
                                                 cell.getStringValue()));
                }
              else if(cell.isBoolean())
                {
                  mp_mcqrMetadataLine->m_otherData.insert(
                    std::pair<QString, QVariant>(m_cols.at(m_colNumber),
                                                 cell.getBooleanValue()));
                }
              else
                {
                  mp_mcqrMetadataLine->m_otherData.insert(
                    std::pair<QString, QVariant>(m_cols.at(m_colNumber),
                                                 cell.toString()));
                }
            }
        }
      m_colNumber++;
    }
}

void
MetadataOdsHandler::endLine()
{
  if(m_rowNumber != 0)
    {
      msp_project->getMcqrExperimentSp().get()->addNewMcqrMetadataLine(
        mp_mcqrMetadataLine);
    }
  m_rowNumber++;
}

void
MetadataOdsHandler::endSheet()
{
}

void
MetadataOdsHandler::endDocument()
{
}
