/**
 * \file input/pepxmlsaxhandler.h
 * \date 18/6/2018
 * \author Olivier Langella
 * \brief parse pepxml result file
 */


/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#ifndef PEPXMLSAXHANDLER_H
#define PEPXMLSAXHANDLER_H

#include <QXmlDefaultHandler>
#include <pappsomspp/pappsoexception.h>
#include "../core/proteinxtp.h"
#include "../core/peptidextp.h"
#include <pappsomspp/amino_acid/aamodification.h>
#include "../core/project.h"
#include "../core/proteinmatch.h"

class PepXmlSaxHandler : public QXmlDefaultHandler
{
  public:
  PepXmlSaxHandler(Project *p_project,
                   IdentificationGroup *p_identification_group,
                   IdentificationDataSource *p_identification_data_source);
  ~PepXmlSaxHandler();

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes);

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName);

  bool startDocument();

  bool endDocument();

  bool characters(const QString &str);

  bool fatalError(const QXmlParseException &exception);
  bool error(const QXmlParseException &exception);

  QString errorString() const;


  private:
  bool startElement_msms_pipeline_analysis(QXmlAttributes attrs);
  bool startElement_msms_run_summary(QXmlAttributes attributes);
  bool startElement_search_database(QXmlAttributes attributes);
  bool startElement_search_summary(QXmlAttributes attributes);
  bool startElement_spectrum_query(QXmlAttributes attributes);
  bool startElement_search_hit(QXmlAttributes attributes);
  bool startElement_alternative_protein(QXmlAttributes attributes);
  bool startElement_peptideprophet_result(QXmlAttributes attributes);
  bool startElement_interprophet_result(QXmlAttributes attributes);
  bool startElement_search_score(QXmlAttributes attributes);
  bool startElement_mod_aminoacid_mass(QXmlAttributes attributes);
  bool startElement_modification_info(QXmlAttributes attributes);

  bool endElement_search_hit();
  bool endElement_modification_info();

  private:
  std::vector<QString> _tag_stack;
  QString _errorStr;
  QString _current_text;

  Project *_p_project;
  IdentificationGroup *_p_identification_group;
  IdentificationDataSource *_p_identification_data_source;
  MsRunSp _sp_msrun;

  std::vector<ProteinMatch *> _p_protein_match_list;
  PeptideEvidence *_p_peptide_evidence;
  PeptideMatch _current_peptide_match;
  PeptideXtpSp _current_peptide_sp;

  QString _current_search_engine;
  QString _current_complete_msrun_file_path;
  unsigned int _scan;
  unsigned int _current_charge;
  pappso::pappso_double _current_retention_time;
  pappso::pappso_double _current_precursor_neutral_mass;
};

#endif // PEPXMLSAXHANDLER_H
