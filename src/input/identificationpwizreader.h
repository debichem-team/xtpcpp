/**
 * \file input/identificationpwizreader.h
 * \date 17/6/2017
 * \author Olivier Langella
 * \brief read identification files using pwiz library
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QFileInfo>
#include "../core/identification_sources/identificationdatasource.h"
#include "../core/project.h"
#include "../core/identificationgroup.h"
#include "../utils/types.h"
//#include <pwiz/data/identdata/IdentData.hpp>


namespace pwiz
{
namespace identdata
{
class IdentDataFile;
}
} // namespace pwiz

class IdentificationPwizReader
{
  public:
  IdentificationPwizReader(const QFileInfo &filename);
  virtual ~IdentificationPwizReader();
  const QString getMsrunName() const;
  IdentificationEngine getIdentificationEngine() const;

  void read(IdentificationDataSource *p_identification_data_source,
            Project *p_project,
            IdentificationGroup *p_identification_group);

  private:
  IdentificationEngine getIdentificationEngine(const QString &xml_id) const;
  IdentificationEngine getIdentificationEngineBySpectrumIdentificationListPtr(
    pwiz::identdata::SpectrumIdentificationListPtr spectrum_ident_list_ptr)
    const;
  IdentificationEngine getIdentificationEngine(
    const pwiz::identdata::AnalysisSoftwarePtr analysis_software_p) const;

  private:
  QFileInfo _identfile;
  pwiz::identdata::IdentDataFile *_pwiz_ident_data_file;
  Project *_p_project;
  IdentificationGroup *_p_identification_group;
  IdentificationDataSource *_p_identification_data_source;
  MsRunSp _sp_msrun;

  std::map<QString, IdentificationEngine> _identification_engine_list;
};
