/**
 * \file input/xtandemsaxhandler.h
 * \date 5/4/2017
 * \author Olivier Langella
 * \brief parse XML X!Tandem result file
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include <QXmlDefaultHandler>
#include <pappsomspp/pappsoexception.h>
#include "../core/proteinxtp.h"
#include "../core/peptidextp.h"
#include <pappsomspp/amino_acid/aamodification.h>
#include "../core/project.h"
#include "../core/proteinmatch.h"
#include "../core/identification_sources/identificationxtandemfile.h"

class XtandemSaxHandler : public QXmlDefaultHandler
{
  public:
  XtandemSaxHandler(Project *p_project,
                    IdentificationGroup *p_identification_group,
                    IdentificationXtandemFile *p_identification_data_source);
  ~XtandemSaxHandler();

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes);

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName);

  bool startDocument();

  bool endDocument();

  bool characters(const QString &str);

  bool fatalError(const QXmlParseException &exception);
  bool error(const QXmlParseException &exception);

  QString errorString() const;


  private:
  bool startElement_group(QXmlAttributes attrs);
  bool startElement_protein(QXmlAttributes attributes);
  bool startElement_note(QXmlAttributes attributes);
  bool startElement_file(QXmlAttributes attributes);
  bool startElement_aa(QXmlAttributes attributes);
  bool startElement_domain(QXmlAttributes attributes);
  bool endElement_domain();
  bool endElement_note();

  private:
  std::vector<QString> _tag_stack;
  QString _errorStr;
  QString _current_text;

  Project *_p_project;
  IdentificationGroup *_p_identification_group;
  IdentificationXtandemFile *_p_identification_data_source;
  MsRunSp _sp_msrun;

  ProteinMatch *_p_protein_match;
  PeptideEvidence *_p_peptide_evidence;
  PeptideMatch _current_peptide_match;
  ProteinXtp _current_protein;
  PeptideXtpSp _current_peptide_sp;

  QMap<QString, pappso::AaModificationP> _map_massstr_aamod;

  QString _current_group_label;
  QString _current_group_type;
  QString _current_note_label;
  QString _current_note_type;
  unsigned int _scan;
  pappso::pappso_double _mhplus_obser;
  unsigned int _charge;
  pappso::pappso_double _retention_time;
  bool _is_protein_description = false;
};
