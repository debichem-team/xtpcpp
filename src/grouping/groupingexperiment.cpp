
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "groupingexperiment.h"
#include <pappsomspp/exception/exceptionnotimplemented.h>
#include "groupingpeptidemass.h"

GroupingExperiment::GroupingExperiment(
  ContaminantRemovalMode contaminantRemovalMode,
  WorkMonitorInterface *p_work_monitor)
{
  _p_work_monitor          = p_work_monitor;
  m_contaminantRemovalMode = contaminantRemovalMode;
}

GroupingExperiment::~GroupingExperiment()
{
}

GroupingExperiment *
GroupingExperiment::newInstance(ContaminantRemovalMode contaminantRemovalMode,
                                const GroupingType &grouping_type,
                                WorkMonitorInterface *p_work_monitor)
{
  if(grouping_type == GroupingType::PeptideMass)
    {
      return new GroupingPeptideMass(contaminantRemovalMode, p_work_monitor);
    }
  else
    {
      throw pappso::ExceptionNotImplemented(
        QObject::tr("Grouping algorithm not yet implemented"));
    }
}


ContaminantRemovalMode
GroupingExperiment::getContaminantRemovalMode() const
{
  return m_contaminantRemovalMode;
}
