/**
 * \file grouping/ptm/ptmislandgroup.h
 * \date 29/5/2017
 * \author Olivier Langella
 * \brief object to group subgroups of ptmislands sharing at least one protein
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PTMISLANDGROUP_H
#define PTMISLANDGROUP_H

#include <memory>
#include "ptmislandsubgroup.h"

class PtmIslandGroup;

/** \brief shared pointer on a Peptide object
 */
typedef std::shared_ptr<PtmIslandGroup> PtmIslandGroupSp;


class PtmIslandGroup
{
  public:
  PtmIslandGroup(PtmIslandSubgroupSp ptm_island_subgroup_sp);
  PtmIslandGroup(const PtmIslandGroup &other);
  ~PtmIslandGroup();
  bool operator<(const PtmIslandGroup &other) const;
  /** @brief merge with the given ptm island subgroup if at least one protein is
   * shared
   * */
  bool mergePtmIslandSubgroupSp(PtmIslandSubgroupSp ptm_island_subgroup_sp);

  unsigned int maxCountSampleScan() const;
  QString getFirstAccession() const;
  void setGroupNumber(unsigned int number);
  unsigned int getGroupNumber() const;

  private:
  std::vector<PtmIslandSubgroupSp> _ptm_island_subgroup_list;
  unsigned int _group_number;
};

#endif // PTMISLANDGROUP_H
