/**
 * \file grouping/ptm/ptminterface.cpp
 * \date 15/06/2020
 * \author Olivier Langella
 * \brief PTM interface
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "ptminterface.h"

PtmInterface::PtmInterface()
{
}

PtmInterface::~PtmInterface()
{
}

std::size_t
PtmInterface::countPeptideMatchPtm(const PeptideMatch &peptide_match) const
{
  return getPtmPositions(peptide_match).size();
}


std::vector<unsigned int>
PtmInterface::getPtmPositions(const ProteinMatch *protein_match) const
{
  std::vector<unsigned int> position_list;

  for(const PeptideMatch &peptide_match :
      protein_match->getPeptideMatchList(ValidationState::validAndChecked))
    {
      unsigned int start = peptide_match.getStart();
      std::vector<unsigned int> positionb_list =
        this->getPtmPositions(peptide_match);
      for(unsigned int position : positionb_list)
        {
          position_list.push_back(start + position);
        }
    }
  std::sort(position_list.begin(), position_list.end());
  auto last = std::unique(position_list.begin(), position_list.end());
  position_list.erase(last, position_list.end());
  return position_list;
}
