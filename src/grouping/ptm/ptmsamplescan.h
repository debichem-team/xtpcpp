/**
 * \file grouping/ptm/ptmsamplescan.h
 * \date 23/7/2017
 * \author Olivier Langella
 * \brief collection of PTM PeptideMatch sharing the same spectrum (sample scan)
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PTMSAMPLESCAN_H
#define PTMSAMPLESCAN_H

#include <memory>
#include "../../core/peptidematch.h"

class PtmGroupingExperiment;
class PtmSampleScan;

/** \brief shared pointer on a PtmSampleScan object
 */
typedef std::shared_ptr<PtmSampleScan> PtmSampleScanSp;


class PtmSampleScan
{
  public:
  PtmSampleScan(const PeptideMatch &peptide_match);
  PtmSampleScan(const PtmSampleScan &other);
  ~PtmSampleScan();

  /** @brief get the best peptide Match (Evalue)
   * peptide match are sorted in this sample/scan by Evalue
   * The representative peptide match is the first in the sorted array
   */
  const PeptideMatch &getRepresentativePeptideMatch() const;
  bool add(const PeptideMatch &peptide_match);
  std::vector<unsigned int> getBestPtmPositionList(
    const PtmGroupingExperiment *p_ptm_grouping_experiment) const;

  /** @brief get all observed PTM positions for this sample/scan
   * positions are intended in a peptide (not protein), from 0 (first amino
   * acid) to size-1 (last amino acid)
   */
  std::vector<unsigned int> getObservedPtmPositionList(
    const PtmGroupingExperiment *p_ptm_grouping_experiment) const;
  const QString
  getHtmlSequence(const PtmGroupingExperiment *p_ptm_grouping_experiment) const;
  std::vector<PeptideMatch> &getPeptideMatchList();

  private:
  std::vector<PeptideMatch> _peptide_match_list;
  static QColor _color_best;
  static QColor _color_other_best;
  static QColor _color_no_best;
};

Q_DECLARE_METATYPE(PtmSampleScan *)
#endif // PTMSAMPLESCAN_H
