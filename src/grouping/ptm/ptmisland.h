/**
 * \file grouping/ptm/ptmisland.h
 * \date 24/5/2017
 * \author Olivier Langella
 * \brief store overlapping peptides containing PTMs
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PTMISLAND_H
#define PTMISLAND_H

#include "../../core/proteinmatch.h"
#include "../../core/peptidematch.h"
#include "ptmsamplescan.h"
#include <memory>

class PtmGroupingExperiment;
class PtmIslandSubgroup;

class PtmIsland;

/** \brief shared pointer on a Peptide object
 */
typedef std::shared_ptr<PtmIsland> PtmIslandSp;

class PtmIsland
{
  public:
  PtmIsland(ProteinMatch *p_protein_match, unsigned int position);
  PtmIsland(const PtmIsland &other);
  ~PtmIsland();
  void addPeptideMatch(const PeptideMatch &peptide_match);

  /** @brief merge with the given ptm island if there is at least one common
   * peptide match
   * */
  bool merge(PtmIslandSp ptm_island_sp);

  bool containsPeptideMatch(const PeptideMatch &element) const;
  std::vector<std::size_t> getSampleScanSet() const;
  ProteinMatch *getProteinMatch();
  unsigned int getProteinStartPosition() const;
  void setPtmIslandSubgroup(PtmIslandSubgroup *p_ptm_island_subgroup);
  void setProteinNumber(unsigned int prot_number);


  const QString getGroupingId() const;
  const std::vector<unsigned int> &getPositionList() const;
  const PtmIslandSubgroup *getPtmIslandSubroup() const;
  std::size_t countSequence() const;
  unsigned int countSampleScanMultiPtm(
    const PtmGroupingExperiment *p_ptm_grouping_experiment) const;
  std::size_t countSampleScanWithPtm(
    const PtmGroupingExperiment *p_ptm_grouping_experiment) const;
  unsigned int getStart() const;
  unsigned int size() const;
  void setChecked(bool check_status);
  bool isChecked() const;

  std::vector<PtmSampleScanSp> getPtmSampleScanSpList() const;

  const std::vector<PeptideMatch> &getPeptideMatchList() const;

  private:
  ProteinMatch *_protein_match_p;
  std::vector<PeptideMatch> _peptide_match_list;
  // std::vector<std::size_t> _sample_scan_set;
  std::vector<unsigned int> _position_list;
  unsigned int _protein_stop  = 0;
  unsigned int _protein_start = 0;
  /** @brief protein rank number in the PTM subgroup
   * */
  unsigned int _prot_number                 = 0;
  bool m_checked                            = true;
  PtmIslandSubgroup *_ptm_island_subgroup_p = nullptr;
};

#endif // PTMISLAND_H
