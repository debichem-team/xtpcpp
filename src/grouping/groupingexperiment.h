
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#include "../utils/types.h"
#include "../utils/workmonitor.h"
#include <pappsomspp/grouping/grpexperiment.h>

#pragma once

class ProteinMatch;
class PeptideEvidence;

class GroupingExperiment
{
  public:
  GroupingExperiment(ContaminantRemovalMode contaminantRemovalMode,
                     WorkMonitorInterface *p_work_monitor);
  virtual ~GroupingExperiment();

  static GroupingExperiment *
  newInstance(ContaminantRemovalMode contaminantRemovalMode,
              const GroupingType &grouping_type,
              WorkMonitorInterface *p_work_monitor);

  virtual pappso::GrpProteinSp &
  getGrpProteinSp(ProteinMatch *p_protein_match) = 0;
  virtual pappso::GrpPeptideSp &
  setGrpPeptide(pappso::GrpProteinSp proteinSp,
                PeptideEvidence *p_peptide_evidence) = 0;
  virtual void
  addPostGroupingGrpProteinSpRemoval(pappso::GrpProteinSp sp_protein) = 0;
  virtual void startGrouping()                                        = 0;
  ContaminantRemovalMode getContaminantRemovalMode() const;

  protected:
  WorkMonitorInterface *_p_work_monitor;
  ContaminantRemovalMode m_contaminantRemovalMode =
    ContaminantRemovalMode::groups;
};
