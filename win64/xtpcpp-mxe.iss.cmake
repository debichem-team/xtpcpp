[Setup]
AppName=X!TandemPipeline

#define public winerootdir "z:"
; Set version number below
#define public version "${XTPCPP_VERSION}"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "z:/home/langella/developpement/git/xtandempipeline/"
#define cmake_build_dir "z:/home/langella/developpement/git/xtandempipeline/wbuild"

; Set version number below
AppVerName=X!TandemPipeline version {#version}
DefaultDirName={pf}\xtandempipeline
DefaultGroupName=xtandempipeline
OutputDir="{#cmake_build_dir}"

; Set version number below
OutputBaseFilename=xtpcpp-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=xtpcpp-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\COPYING"
AppCopyright="Copyright (C) 2016- Olivier Langella"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="X!TandemPipeline, by Olivier Langella"
AppContact="Olivier Langella, research engineer at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
; WizardImageFile="{#sourceDir}\images\splashscreen-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}"

[Files]
Source: "z:/win64/mxe_dll/*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/win64/mxe/usr/x86_64-w64-mingw32.shared/qt5/plugins/*"; DestDir: {app}/plugins; Flags: ignoreversion recursesubdirs;
Source: "z:/win64/mxe/usr/x86_64-w64-mingw32.shared/lib/grantlee/5.2/*"; DestDir: {app}/grantlee/5.2; Flags: ignoreversion recursesubdirs;
Source: "z:/win64/librdata/.libs/librdata-0.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/alglib/wbuild/libalglib.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/libpwizlite/wbuild/src/libpwizlite.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/libodsstream/wbuild/src/libodsstream.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/pappsomspp/wbuild/src/libpappsomspp.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/pappsomspp/wbuild/src/pappsomspp/widget/libpappsomspp-widget.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}\README"; DestDir: {app}; Flags: isreadme; Components: xtpcppComp
Source: "{#sourceDir}\COPYING"; DestDir: {app}; Flags: isreadme; Components: xtpcppComp
Source: "{#sourceDir}\win64\xtandempipeline_icon.ico"; DestDir: {app}; Components: xtpcppComp
Source: "{#cmake_build_dir}\xtpcpp_user_manual.pdf"; DestDir: {app};

Source: "{#cmake_build_dir}\src\xtpcpp.exe"; DestDir: {app}; Components: xtpcppComp
Source: "{#cmake_build_dir}\src\tandemwrapper.exe"; DestDir: {app}; Components: xtpcppComp
Source: "{#cmake_build_dir}\src\mzxmlconverter.exe"; DestDir: {app}; Components: xtpcppComp

[Icons]
Name: "{group}\xtpcpp"; Filename: "{app}\xtpcpp.exe"; WorkingDir: "{app}";IconFilename: "{app}\xtandempipeline_icon.ico"
Name: "{group}\Uninstall xtpcpp"; Filename: "{uninstallexe}"

[Types]
Name: "xtpcppType"; Description: "Full installation"

[Components]
Name: "xtpcppComp"; Description: "X!TandemPipeline files"; Types: xtpcppType

[Run]
;Filename: "{app}\README"; Description: "View the README file"; Flags: postinstall shellexec skipifsilent
Filename: "{app}\xtpcpp.exe"; Description: "Launch X!TandemPipeline"; Flags: postinstall nowait unchecked
