
# cd buildwin64
# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win64/home.cmake ..

#set(ODSSTREAM_QT5_FOUND 1)
#set(ODSSTREAM_INCLUDE_DIR "/home/olivier/eclipse/git/libodsstream/src")
#set(ODSSTREAM_QT5_LIBRARY "/home/olivier/eclipse/git/libodsstream/cbuild/src/libodsstream-qt5.so")

set(PAPPSOMSPP_QT5_FOUND 1)
set(PAPPSOMSPP_WIDGET_QT5_FOUND 1)
set(PAPPSOMSPP_INCLUDE_DIR "/home/olivier/eclipse/git/pappsomspp/src")
set(PAPPSOMSPP_QT5_LIBRARY "/home/olivier/eclipse/git/pappsomspp/cbuild/src/libpappsomspp.so")
set(PAPPSOMSPP_WIDGET_QT5_LIBRARY "/home/olivier/eclipse/git/pappsomspp/cbuild/src/pappsomspp/widget/libpappsomspp-widget.so")



if(NOT TARGET PappsoMSpp::Core)
        add_library(PappsoMSpp::Core UNKNOWN IMPORTED)
        set_target_properties(PappsoMSpp::Core PROPERTIES
            IMPORTED_LOCATION             "${PAPPSOMSPP_QT5_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${PAPPSOMSPP_INCLUDE_DIR}")
endif()


if(NOT TARGET PappsoMSpp::Widget)
        add_library(PappsoMSpp::Widget UNKNOWN IMPORTED)
        set_target_properties(PappsoMSpp::Widget PROPERTIES
            IMPORTED_LOCATION             "${PAPPSOMSPP_WIDGET_QT5_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${PAPPSOMSPP_INCLUDE_DIR}")
endif()


   
#if(NOT TARGET OdsStream::Core)
#    add_library(OdsStream::Core UNKNOWN IMPORTED)
#    set_target_properties(OdsStream::Core PROPERTIES
#            IMPORTED_LOCATION             "${ODSSTREAM_QT5_LIBRARY}"
#            INTERFACE_INCLUDE_DIRECTORIES "${ODSSTREAM_INCLUDE_DIR}"
#    )
#endif()
