Source: xtpcpp
Section: science
Priority: optional
Maintainer: The Debichem Group <debichem-devel@alioth-lists.debian.net>
Uploaders: Filippo Rusconi <lopippo@debian.org>
Build-Depends: debhelper-compat (= 12),
 dpkg-dev (>= 1.18.25),
 cmake (>= 3.12),
 qtbase5-dev,
 libpappsomspp-dev (>= @LIBPAPPSOMSPP_VERSION@),
 libpappsomspp-widget-dev (>= @LIBPAPPSOMSPP_VERSION@),
 libodsstream-dev,
 libqt5svg5-dev,
 libqcustomplot-dev,
 libgrantlee5-dev,
 librdata-dev (>= @LIBRDATA_VERSION@),
 docbook-to-man,
 libjs-jquery,
 libjs-highlight.js,
 daps,
 fonts-ebgaramond,
 fonts-ebgaramond-extra,
 doxygen,
 catch2,
 graphviz
Standards-Version: 4.6.0
Homepage: http://pappso.inra.fr/bioinfo

Package: xtpcpp
Architecture: any
Multi-Arch: no
Depends: ${shlibs:Depends},
 ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Recommends: xtpcpp-tools
Description: C++ version of X!TandemPipeline
 The program allows one to perform the following tasks:
  -Reads X!Tandem xml results files
  -Reads MASCOT dat results files
  -Reads TPP pepXML results files
  -Reads PSI mzIdentML results files
  -Run X!Tandem analyzes through a graphical user interface
  -Implements various filters based on statistical values
  -Powerful original grouping algorithm to filter redundancy
  -Phosphopeptide mode to handle phosphoproteomics datasets
  -Edit, search and sort the data graphically
  -XIC chromatogram browser (eXtracted Ion Current)
  -Comparisons of theoretical isotope patterns to measured MS1 XIC areas
  -Export data directly to Microsoft Office 2010 and LibreOffice (ods export)
  -Handle huge datasets very quickly
  -Perform peptide quantification through MassChroQml export

Package: xtpcpp-doc
Section: doc
Architecture: all
Depends: libjs-jquery,
         libjs-highlight.js,
         ${misc:Depends}
Description: C++ version of X!TandemPipeline (user manual)
 The program allows one to perform the following tasks:
  -Reads X!Tandem xml results files
  -Reads MASCOT dat results files
  -Reads TPP pepXML results files
  -Reads PSI mzIdentML results files
  -Run X!Tandem analyzes through a graphical user interface
  -Implements various filters based on statistical values
  -Powerful original grouping algorithm to filter redundancy
  -Phosphopeptide mode to handle phosphoproteomics datasets
  -Edit, search and sort the data graphically
  -XIC chromatogram browser (eXtracted Ion Current)
  -Comparisons of theoretical isotope patterns to measured MS1 XIC areas
  -Export data directly to Microsoft Office 2010 and LibreOffice (ods export)
  -Handle huge datasets very quickly
  -Perform peptide quantification through MassChroQml export
 .
 This package ships the user manual in both PDF and HTML formats.

Package: xtpcpp-tools
Architecture: any
Multi-Arch: no
Depends: ${shlibs:Depends},
 ${misc:Depends},
 tandem-mass
Provides: xtpcpp-tools
Conflicts: xtpcpp-tandemwrapper-dbgsym,
  xtpcpp-tandemwrapper
Replaces: xtpcpp-tandemwrapper-dbgsym,
  xtpcpp-tandemwrapper,
Description: X!Tandem wrapper for timsTOF pro and modern mzML data
 The program allows one to perform the following tasks:
  -Reads X!Tandem xml results files
  -Reads MASCOT dat results files
  -Reads TPP pepXML results files
  -Reads PSI mzIdentML results files
  -Run X!Tandem analyzes through a graphical user interface
  -Implements various filters based on statistical values
  -Powerful original grouping algorithm to filter redundancy
  -Phosphopeptide mode to handle phosphoproteomics datasets
  -Edit, search and sort the data graphically
  -XIC chromatogram browser (eXtracted Ion Current)
  -Comparisons of theoretical isotope patterns to measured MS1 XIC areas
  -Export data directly to Microsoft Office 2010 and LibreOffice (ods export)
  -Handle huge datasets very quickly
  -Perform peptide quantification through MassChroQml export
 .
 This package ships two command line tools that are useful when
 mass data are available in a format that is not immediately consumable by 
 X!Tandem (like mzML or Bruker timsTOF data).

