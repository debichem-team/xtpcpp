message("\n${BoldRed}WIN10-MINGW64 environment${ColourReset}\n")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../development")


set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/include")
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/bin")

# The TARGET changes according to the plaform
# For example, it changes to XtpCpp for macOS.

SET(TARGET xtpcpp)

# We do not build the tests under Win10.
set (MAKE_TEST 0)


if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()

message(STATUS "${BoldGreen}Setting definition -DPMSPP_LIBRARY for symbol DLL export.${ColourReset}")
add_definitions(-DPMSPP_LIBRARY)


find_package(ZLIB REQUIRED)


if(MAKE_TEST)

	set(Quazip5_FOUND 1)
	set(Quazip5_INCLUDE_DIRS "${HOME_DEVEL_DIR}/quazip5/development")
	set(Quazip5_LIBRARY "${HOME_DEVEL_DIR}/quazip5/build-area/mingw64/libquazip5.dll") 
	if(NOT TARGET Quazip5::Quazip5)
		add_library(Quazip5::Quazip5 UNKNOWN IMPORTED)
		set_target_properties(Quazip5::Quazip5 PROPERTIES
			IMPORTED_LOCATION             "${Quazip5_LIBRARY}"
			INTERFACE_INCLUDE_DIRECTORIES "${Quazip5_INCLUDE_DIRS}")
	endif()

endif()


set(QCustomPlot_FOUND 1)
set(QCustomPlot_INCLUDE_DIRS "${HOME_DEVEL_DIR}/qcustomplot/development")
# Note the QCustomPlot_LIBRARIES (plural) because on Debian, the
# QCustomPlotConfig.cmake file has this variable name (see the unix-specific
# toolchain file.
set(QCustomPlot_LIBRARIES "${HOME_DEVEL_DIR}/qcustomplot/build-area/mingw64/libqcustomplot.dll") 
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
if(NOT TARGET QCustomPlot::QCustomPlot)
	add_library(QCustomPlot::QCustomPlot UNKNOWN IMPORTED)
	set_target_properties(QCustomPlot::QCustomPlot PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlot_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlot_INCLUDE_DIRS}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY
		)
endif()


set(Alglib_FOUND 1)
set(Alglib_INCLUDE_DIRS "${HOME_DEVEL_DIR}/alglib/development/src")
set(Alglib_LIBRARY "${HOME_DEVEL_DIR}/alglib/build-area/mingw64/libalglib.dll") 
if(NOT TARGET Alglib::Alglib)
	add_library(Alglib::Alglib UNKNOWN IMPORTED)
	set_target_properties(Alglib::Alglib PROPERTIES
		IMPORTED_LOCATION             "${Alglib_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${Alglib_INCLUDE_DIRS}")
endif()


# No more used since the source code is now part of our code tree.
#set(CustomPwiz_FOUND 1)
#set(CustomPwiz_INCLUDE_DIRS "${HOME_DEVEL_DIR}/custompwiz/development/src")
#set(CustomPwiz_LIBRARY "${HOME_DEVEL_DIR}/custompwiz/build-area/src/libcustompwiz.dll.a") 
#if(NOT TARGET CustomPwiz::CustomPwiz)
#add_library(CustomPwiz::CustomPwiz UNKNOWN IMPORTED)
#set_target_properties(CustomPwiz::CustomPwiz PROPERTIES
#IMPORTED_LOCATION             "${CustomPwiz_LIBRARY}"
#INTERFACE_INCLUDE_DIRECTORIES "${CustomPwiz_INCLUDE_DIRS}")
#endif()


find_package(Boost COMPONENTS iostreams thread filesystem chrono REQUIRED ) 


set(Zstd_FOUND 1)
set(Zstd_INCLUDE_DIRS "c:/msys64/mingw64/include")
set(Zstd_LIBRARY "c:/msys64/mingw64/bin/libzstd.dll") 
if(NOT TARGET Zstd::Zstd)
	add_library(Zstd::Zstd UNKNOWN IMPORTED)
	set_target_properties(Zstd::Zstd PROPERTIES
		IMPORTED_LOCATION             "${Zstd_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${Zstd_INCLUDE_DIRS}")
endif()


set(SQLite3_FOUND 1)
set(SQLite3_INCLUDE_DIRS "c:/msys64/mingw64/include")
set(SQLite3_LIBRARY "c:/msys64/mingw64/bin/libsqlite3-0.dll") 
if(NOT TARGET SQLite::SQLite3)
	add_library(SQLite::SQLite3 UNKNOWN IMPORTED)
	set_target_properties(SQLite::SQLite3 PROPERTIES
		IMPORTED_LOCATION             "${SQLite3_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${SQLite3_INCLUDE_DIRS}")
endif()


set(OdsStream_QT5_FOUND 1)
set(OdsStream_INCLUDE_DIR "${HOME_DEVEL_DIR}/odsstream/src")
set(OdsStream_QT5_LIBRARY "${HOME_DEVEL_DIR}/odsstream/build-area/mingw64/src/libodsstream-qt5.dll")
if(NOT TARGET OdsStream::Core)
	add_library(OdsStream::Core UNKNOWN IMPORTED)
	set_target_properties(OdsStream::Core PROPERTIES
		IMPORTED_LOCATION             "${OdsStream_QT5_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${OdsStream_INCLUDE_DIR}"
		)
endif()


# On Win10 all the code is relocatable.
remove_definitions(-fPIC)
