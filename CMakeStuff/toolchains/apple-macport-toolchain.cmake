message("APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")


set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "/opt/local/include")
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "/opt/local/lib")

# This is used throughout all the build system files
set(TARGET XtpCpp)

# Now that we know what is the TARGET (in the toolchain files above,
# we can compute the lowercase TARGET (used for string replacements in 
# configure files and also for the resource compilation with windres.exe.
string(TOLOWER ${TARGET} TARGET_LOWERCASE)
message("TARGET_LOWERCASE: ${TARGET_LOWERCASE}")


set(HOME_DEVEL_DIR "/Users/rusconi/devel")

set(LINKER_FLAGS "${LINKER_FLAGS} -Wc++17-compat")

set(CMAKE_MACOSX_RPATH 1)
#set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")

add_definitions("-I/usr/include -I/usr/local/include")

### Define the MacOSX application bundle

# Copy the icon file to the Contents/Resources directory of the bundle at
# location ${MACOSX_PACKAGE_LOCATION}.
set_source_files_properties(${CMAKE_SOURCE_DIR}/src/resources/xtandempipeline_icon.icns 
  PROPERTIES MACOSX_PACKAGE_LOCATION Resources)

set(MACOSX_BUNDLE_BUNDLE_NAME ${TARGET}.app)
message(STATUS "MACOSX_BUNDLE_BUNDLE_NAME: ${MACOSX_BUNDLE_BUNDLE_NAME}")

# The bundle directory will be in the binary directory matching the
# src directory of the main source tree.
set(BUNDLE_DIR ${CMAKE_BINARY_DIR}/src/${MACOSX_BUNDLE_BUNDLE_NAME})
message("BUNDLE_DIR: ${BUNDLE_DIR}")

## INSTALL directories
# Install the files in the bundle.
set(CMAKE_INSTALL_PREFIX "${BUNDLE_DIR}/Contents")
message("CMAKE_INSTALL_PREFIX: ${CMAKE_INSTALL_PREFIX}")

set(BIN_DIR ${BUNDLE_DIR}/Contents/MacOS)
set(DOC_DIR ${BUNDLE_DIR}/Contents/doc)

set(MACOSX_BUNDLE_BUNDLE_EXECUTABLE_NAME ${TARGET})
set(MACOSX_BUNDLE_COPYRIGHT ${COPYRIGHT})
set(MACOSX_BUNDLE_ICON_FILE ../../src/resources/xtandempipeline_icon.icns)
set(MACOSX_BUNDLE_GUI_IDENTIFIER ${IDENTIFIER})
set(MACOSX_BUNDLE_LONG_VERSION_STRING ${VERSION})
set(MACOSX_BUNDLE_BUNDLE_NAME ${TARGET})
set(MACOSX_BUNDLE_SHORT_VERSION_STRING ${VERSION})
set(MACOSX_BUNDLE_BUNDLE_VERSION ${VERSION})


set(QCustomPlot_FOUND 1)
set(QCustomPlot_INCLUDE_DIRS "${HOME_DEVEL_DIR}/qcustomplot/development")
set(QCustomPlot_LIBRARIES "${HOME_DEVEL_DIR}/qcustomplot/build-area/mac/libqcustomplot.dylib") 
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
if(NOT TARGET QCustomPlot::QCustomPlot)
	add_library(QCustomPlot::QCustomPlot UNKNOWN IMPORTED)
	set_target_properties(QCustomPlot::QCustomPlot PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlot_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlot_INCLUDE_DIRS}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY
		)
endif()


set(Grantlee5_FOUND 1)
set(Grantlee5_INCLUDE_DIRS "/opt/local/include")
set(Grantlee5_LIBRARIES "/opt/local/lib/libgrantlee_core.dylib") 
if(NOT TARGET Grantlee5::Templates)
  add_library(Grantlee5::Templates UNKNOWN IMPORTED)
  set_target_properties(Grantlee5::Templates PROPERTIES
    IMPORTED_LOCATION             "${Grantlee5_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${Grantlee5_INCLUDE_DIRS}")
endif()


set(RData_FOUND 1)
set(RData_INCLUDE_DIRS "${HOME_DEVEL_DIR}/rdata/development/src")
set(RData_LIBRARIES "${HOME_DEVEL_DIR}/rdata/build-area/mac/src/librdata.dylib") 
if(NOT TARGET RData::Core)
  add_library(RData::Core UNKNOWN IMPORTED)
  set_target_properties(RData::Core PROPERTIES
    IMPORTED_LOCATION             "${RData_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${RData_INCLUDE_DIRS}")
endif()


set(OdsStream_FOUND 1)
set(OdsStream_INCLUDE_DIRS "${HOME_DEVEL_DIR}/odsstream/development/src")
set(OdsStream_LIBRARIES "${HOME_DEVEL_DIR}/odsstream/build-area/mac/src/libodsstream.dylib")
if(NOT TARGET OdsStream::Core)
  add_library(OdsStream::Core UNKNOWN IMPORTED)
  set_target_properties(OdsStream::Core PROPERTIES
    IMPORTED_LOCATION             "${OdsStream_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${OdsStream_INCLUDE_DIRS}")
endif()


set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_INCLUDE_DIRS "${HOME_DEVEL_DIR}/pappsomspp/development/src")
set(PappsoMSpp_LIBRARIES "${HOME_DEVEL_DIR}/pappsomspp/build-area/mac/src/libpappsomspp.dylib")
if(NOT TARGET PappsoMSpp::Core)
  add_library(PappsoMSpp::Core UNKNOWN IMPORTED)
  set_target_properties(PappsoMSpp::Core PROPERTIES
    IMPORTED_LOCATION             "${PappsoMSpp_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIRS}")
endif()


set(PappsoMSppWidget_FOUND 1)
set(PappsoMSppWidget_LIBRARIES "${HOME_DEVEL_DIR}/pappsomspp/build-area/mac/src/pappsomspp/widget/libpappsomspp-widget.dylib")
if(NOT TARGET PappsoMSpp::Widget)
  add_library(PappsoMSpp::Widget UNKNOWN IMPORTED)
  set_target_properties(PappsoMSpp::Widget PROPERTIES
    IMPORTED_LOCATION             "${PappsoMSppWidget_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIRS}")
endif()



set(CMAKE_BUILD_TYPE "release")
add_definitions(-fPIC)

